﻿using HDS.App.Domain.Aggregate;
using HDS.App.Extensions.Decorators;
using HDS.App.Extensions.Static;
using HDS.App.Tests.Models.Azalik;
using System;

namespace HDS.App.Tests.Views.Azalik {

    [ResourceLocator(typeof(Resources.Aza))]
    public class AzaView : AGBase<AzaView> {

        [IDPropertyLocator]
        public long AzaID { get; set; }

        public long AzaKunyeID { get; set; }

        [TableColumn(Order = 1), FormField(EFormType.Insert | EFormType.Edit)]
        public string KunyeIsim { get; set; }

        [TableColumn(Order = 2), FormField(EFormType.Insert | EFormType.Edit)]
        public string KunyeSoyisim { get; set; }

        [TableColumn(Order = 3), FormField(EFormType.Insert | EFormType.Edit)]
        public string KunyeMemleket { get; set; }

        [TableColumn(Order = 4), FormField(EFormType.Insert | EFormType.Edit)]
        public string KunyeIrtibatTelefonu { get; set; }

        [TableColumn(Order = 5), FormField(EFormType.Insert | EFormType.Edit)]
        public DateTime KunyeMevlid { get; set; }

        public EMeslek KunyeMeslek { get; set; }

        [ComputedField(EComputedType.Enum, typeof(EMeslek))]
        [TableColumn(Order = 6)]
        public string KunyeMeslekComputed {
            get {
                return KunyeMeslek.GetEnumResource();
            }
        }

        [TableColumn(Order = 7)]
        public long AzaTCKN { get; set; }

        public string AzaSifre { get; set; }

        public string AzaAccessToken { get; set; }

        public string AzaActivationToken { get; set; }

        public string AzaRecoveryToken { get; set; }

        public DateTime AzaActivationTokenExpiresOn { get; set; }

        public DateTime AzaAccessTokenExpiresOn { get; set; }

        public DateTime AzaRecoveryTokenExpiresOn { get; set; }

        protected override void Map(IAGViewBuilder<AzaView> builder) {

            builder
                .MapsTo(x => x.SchemaName("VW").ViewName("AzaView"))
                .Select<Aza>(li =>
                    li
                    .Map(x => x.ID, x => x.AzaID)
                    .Map(x => x.TCKN, x => x.AzaTCKN)
                    .Map(x => x.Sifre, x => x.AzaSifre)
                    .Map(x => x.AccessToken, x => x.AzaAccessToken)
                    .Map(x => x.ActivationToken, x => x.AzaActivationToken)
                    .Map(x => x.RecoveryToken, x => x.AzaRecoveryToken)
                    .Map(x => x.AccessTokenExpiresOn, x => x.AzaAccessTokenExpiresOn)
                    .Map(x => x.ActivationTokenExpiresOn, x => x.AzaActivationTokenExpiresOn)
                    .Map(x => x.RecoveryTokenExpiresOn, x => x.AzaRecoveryTokenExpiresOn)
                ).OuterJoin<Kunye>(li =>
                    li
                    .Map(x => x.ID, x => x.AzaKunyeID)
                    .Map(x => x.IrtibatTelefonu, x => x.KunyeIrtibatTelefonu)
                    .Map(x => x.Isim, x => x.KunyeIsim)
                    .Map(x => x.Memleket, x => x.KunyeMemleket)
                    .Map(x => x.Meslek, x => x.KunyeMeslek)
                    .Map(x => x.Mevlid, x => x.KunyeMevlid)
                ).On<Aza>((a, b) => a.ID == b.KunyeID);

        }

    }

}
