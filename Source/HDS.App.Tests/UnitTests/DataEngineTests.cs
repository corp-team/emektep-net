﻿using HDS.App.Api.Common;
using HDS.App.Domain.Objects;
using HDS.App.Engines;
using HDS.App.Extensions.Static;
using HDS.App.Tests.Models.Azalik;
using HDS.App.Tests.Static;
using HDS.App.Tests.Views.Azalik;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace HDS.App.Tests.UnitTests {

    [TestClass]
    public class DataEngineTests {

        protected async Task<TResponse> ParseResponse<TResponse>(IHttpActionResult result) {
            var response = await result.ExecuteAsync(new CancellationToken());
            var responseString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<TResponse>(responseString);
        }

        [TestInitialize]
        public void BeforeTest() {
            Builder.Bootstrap();
        }

        [TestMethod]
        public void BuildsDatabase() {
            SDataEngine.WipeDomain(typeof(DataEngineTests).Assembly.GetName(), rebuild: true);
        }

        [TestMethod]
        public async Task MappedDomainFetched() {
            var session = new Session() { UserName = "25871077074", Password = "3132" };
            var eng = SDataEngine.GenerateAGEngine<AzaView>();
            var azaResponse = (await eng.SelectSingle(a => a.AzaTCKN == Convert.ToInt64(session.UserName) && a.AzaSifre == session.Password));
            Assert.IsTrue(azaResponse.Success);
            var azaView = azaResponse.SingleResponse;
            Assert.IsNotNull(azaView);
            var kunye = azaView.GetMappedDomain<Kunye>();
            var aza = azaView.GetMappedDomain<Aza>();
            Assert.IsNotNull(kunye);
            Assert.IsNotNull(aza);
            Assert.AreNotEqual(0, kunye.ID);
            Assert.AreNotEqual(0, aza.ID);
        }


        [TestMethod]
        public async Task ControllerInsertsView() {
            var session = new Session() { UserName = "25871077074", Password = "3132" };
            var kunye = (await SDataEngine.GenerateDOEngine<Kunye>().Insert(new Kunye { IrtibatTelefonu = "4243234234", Isim = "Hüssein", Soyisim = "Sönmez" }))
                .SingleResponse;
            var aza = (await SDataEngine.GenerateDOEngine<Aza>().Insert(new Aza { KunyeID = kunye.ID, Sifre = session.Password, TCKN = Convert.ToInt64(session.UserName) }))
                .SingleResponse;
            var eng = SDataEngine.GenerateAGEngine<AzaView>();
            var azaResponse = (await eng.SelectSingle(a => a.AzaTCKN == Convert.ToInt64(session.UserName) && a.AzaSifre == session.Password));
            Assert.IsTrue(azaResponse.Success, azaResponse.Message);
            var azaView = azaResponse.SingleResponse;
            Assert.IsNotNull(azaView);

            var api = new GenericController(typeof(Aza), typeof(AzaView));
            api.ControllerContext.Configuration = new HttpConfiguration();
            api.Request = new HttpRequestMessage();
            var aggResponse = await ParseResponse<DMLResponse<Aza>>(await api.Insert(azaView, typeof(Kunye)));
            Assert.IsTrue(aggResponse.Success, aggResponse.Message);
        }


        [TestMethod]
        public async Task ControllerEditsView() {
            var session = new Session() { UserName = "25871077074", Password = "3132" };
            var eng = SDataEngine.GenerateAGEngine<AzaView>();
            var azaResponse = (await eng.SelectSingle(a => a.AzaTCKN == Convert.ToInt64(session.UserName) && a.AzaSifre == session.Password));
            Assert.IsTrue(azaResponse.Success, azaResponse.Message);
            var azaView = azaResponse.SingleResponse;
            Assert.IsNotNull(azaView);

            var api = new GenericController(typeof(Aza), typeof(AzaView));
            api.ControllerContext.Configuration = new HttpConfiguration();
            api.Request = new HttpRequestMessage();
            azaView.KunyeIsim = "Huseyn";
            var aggResponse = await ParseResponse<DMLResponse<AzaView>>(await api.Edit(azaView));
            Assert.IsTrue(aggResponse.Success, aggResponse.Message);
            var updatedResponse = (await eng.SelectSingle(a => a.AzaTCKN == Convert.ToInt64("25871077074")));
            Assert.IsTrue(updatedResponse.Success, updatedResponse.Message);
            Assert.AreEqual("Huseyn", updatedResponse.SingleResponse.KunyeIsim);
        }


        [TestMethod]
        public async Task ColumnRequiredOrNot() {
            var session = new Session() { UserName = "25871077074", Password = "3132" };
            var eng = SDataEngine.GenerateAGEngine<AzaView>();
            var azaResponse = (await eng.SelectSingle(a => a.AzaTCKN == Convert.ToInt64(session.UserName) && a.AzaSifre == session.Password));
            Assert.IsTrue(azaResponse.Success);
            var azaView = azaResponse.SingleResponse;
            Assert.IsNotNull(azaView);
            Assert.IsTrue(azaView.IsColumnRequired(new AzaView().ResolveProperties().Single(p => p.Name == "AzaTCKN")));
        }

        [TestMethod]
        public async Task DomainReferenceKeysFetched() {
            var session = new Session() { UserName = "25871077074", Password = "3132" };
            var eng = SDataEngine.GenerateAGEngine<AzaView>();
            var azaResponse = (await eng.SelectSingle(a => a.AzaTCKN == Convert.ToInt64(session.UserName) && a.AzaSifre == session.Password));
            Assert.IsTrue(azaResponse.Success);
            var azaView = azaResponse.SingleResponse;
            Assert.IsNotNull(azaView);
            var aza = azaView.GetMappedDomain<Aza>();
            var kunyeReference = aza.GetRelationKeys().FirstOrDefault();
            Assert.IsNotNull(kunyeReference);
            Assert.AreEqual("KunyeID", kunyeReference.Name);
        }

        [TestMethod]
        public async Task OneFetched() {
            var session = new Session() { UserName = "25871077074", Password = "3132" };
            var eng = SDataEngine.GenerateDOEngine<Aza>();
            var azaResponse = (await eng.SelectSingle(aza => aza.TCKN == Convert.ToInt64(session.UserName) && aza.Sifre == session.Password));
            Assert.IsTrue(azaResponse.Success);
            Assert.IsNotNull(azaResponse.SingleResponse);
        }

        [TestMethod]
        public async Task OneFetchedByID() {
            var session = new Session() { UserName = "25871077074", Password = "3132" };
            var aza = (await SDataEngine.GenerateDOEngine<Aza>().SelectSingle(a => a.TCKN == Convert.ToInt64(session.UserName) && a.Sifre == session.Password))
                .SingleResponse;
            var eng = SDataEngine.GenerateAGEngine<AzaView>();
            var existing = (await eng.SelectByID(aza.ID)).SingleResponse;
            Assert.IsNotNull(existing);
        }

        [TestMethod]
        public async Task OneOfManyFetchedBy() {
            var session = new Session() { UserName = "25871077074", Password = "3132" };
            var eng = SDataEngine.GenerateDOEngine<Aza>();
            var existing = (await eng.SelectBy(aza => aza.TCKN == Convert.ToInt64(session.UserName) && aza.Sifre == session.Password)).CollectionResponse.First();
            Assert.IsNotNull(existing);
        }

        [TestMethod]
        public async Task OneUpdated() {
            var session = new Session() { UserName = "25871077074", Password = "3132", Token = new ApiToken() { AccessToken = Guid.NewGuid().ToString("N") } };

            var eng = SDataEngine.GenerateDOEngine<Aza>();
            var response = (await eng.SelectSingle(aza => aza.TCKN == Convert.ToInt64(session.UserName) && aza.Sifre == session.Password));
            Assert.IsNotNull(response.SingleResponse);

            var updated = await eng.Update(response.SingleResponse.ID, (modifier) => modifier
                .Update(a => a.AccessToken).Set(session.Token.AccessToken)
                .Update(a => a.AccessTokenExpiresOn).Set(DateTime.Now.AddMinutes(11))
                .UpdateThen<Kunye, String>(obj => obj.Isim).Set("Hüseyn").FromWhere<Aza>((a, b) => a.ID == b.KunyeID)
                );
            Assert.IsTrue(updated.Success);
            Assert.AreEqual(session.Token.AccessToken, updated.SingleResponse.AccessToken);
            Assert.AreEqual("Hüseyn", SDataEngine.GenerateDOEngine<Kunye>().SelectSingleByIDSync(updated.SingleResponse.KunyeID).SingleResponse.Isim);
        }

        [TestMethod]
        public async Task OneUpsertedAggregate() {
            var session = new Session() { UserName = "25871077074", Password = "3132" };
            var eng = SDataEngine.GenerateAGEngine<AzaView>();
            var azaResponse = (await eng.SelectSingle(a => a.AzaTCKN == Convert.ToInt64(session.UserName) && a.AzaSifre == session.Password));
            Assert.IsTrue(azaResponse.Success, azaResponse.Message);
            var azaView = azaResponse.SingleResponse;
            Assert.IsNotNull(azaView);
            azaView.AzaActivationToken = "42342342980348309248023492";
            var dmlAgg = await SDataEngine.GenerateDOEngine<Aza>().UpdateAggregate(azaView);
            Assert.IsTrue(dmlAgg.Success, dmlAgg.Message);
            var token =
                (await eng.SelectSingle(a => a.AzaTCKN == Convert.ToInt64(session.UserName) && a.AzaSifre == session.Password)).SingleResponse.AzaActivationToken;
            Assert.AreEqual(azaView.AzaActivationToken, token);
        }

        [TestMethod]
        public void InsertedIfAbsentSync() {
            var kunyeResponse = SDataEngine.GenerateDOEngine<Kunye>().InsertIfAbsentSync(new Kunye() { IrtibatTelefonu = "5360331719", Isim = "Hüseyin", Memleket = "Sivas", Meslek = EMeslek.Hekim, Mevlid = DateTime.Parse("30.03.1981"), Soyisim = "Sönmez" }, k => k.ID);
            var aza = new Aza() { Sifre = "3132", TCKN = Convert.ToInt64("25871077074") };
            Assert.IsTrue(kunyeResponse.Success, kunyeResponse.Message);
            if (kunyeResponse.Success) {
                var kunye = kunyeResponse.SingleResponse;
                aza.KunyeID = kunye.ID;
                var azaResponse = SDataEngine.GenerateDOEngine<Aza>().InsertIfAbsentSync(aza, a => a.TCKN);
                Assert.IsTrue(azaResponse.Success, azaResponse.Message);
                if (azaResponse.Success) {
                    aza = azaResponse.SingleResponse;
                }
                //var deletedAzaResponse = SDataEngine.GenerateDOEngine<Aza>().DeleteSync(aza.ID);
                //Assert.IsTrue(deletedAzaResponse.Success, deletedAzaResponse.Message);
                //var deletedKunyeResponse = SDataEngine.GenerateDOEngine<Kunye>().DeleteSync(kunye.ID);
                //Assert.IsTrue(deletedKunyeResponse.Success, deletedKunyeResponse.Message);
            }
        }
    }

}
