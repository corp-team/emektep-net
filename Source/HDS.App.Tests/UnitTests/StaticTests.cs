﻿using HDS.App.Api.Static;
using HDS.App.Extensions.Static;
using HDS.App.Tests.Models.Azalik;
using HDS.App.Tests.Static;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Globalization;
using System.Linq;

namespace HDS.App.Tests.UnitTests {

    [TestClass]
    public class StaticTests {

        [TestInitialize]
        public void BeforeTest() {
            Builder.Bootstrap();
        }

        [TestMethod]
        public void ToEnum() {
            var e = "Hekim".ToEnum<EMeslek>();
            Assert.AreEqual(EMeslek.Hekim, e);
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.GetCultureInfo("en-US");
            var e1 = "Doctor".ToEnum<EMeslek>();
            Assert.AreEqual(EMeslek.Hekim, e1);
        }

        [TestMethod]
        public void LoadsConfiguration() {

            var apiConfig = SAPIValues.LoadConfiguration();
            Assert.IsTrue(apiConfig.Routes.Count() > 0);
            Assert.IsNotNull(apiConfig.Routes.First().Controller);
            Assert.IsTrue(apiConfig.Routes.First().Methods.Count() > 2);

        }

        [TestMethod]
        public void SlicesPartials() {

            var sentence = "one/{id}/with/{formType}/by/{category}";
            Assert.AreEqual("id", sentence.SlicePartials(begin: '{', end: new char[] { '}' }).First());
            Assert.AreEqual("formType", sentence.SlicePartials(begin: '{', end: new char[] { '}' }).Skip(1).First());
            Assert.AreEqual("category", sentence.SlicePartials(begin: '{', end: new char[] { '}' }).Skip(2).First());

        }

    }

}
