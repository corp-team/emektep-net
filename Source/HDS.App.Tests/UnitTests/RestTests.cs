﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using System.Collections.Specialized;
using HDS.App.Tests.Models.Azalik;
using HDS.App.Engines;
using HDS.App.Api.Static;
using HDS.App.Api.Common;
using HDS.App.Api.APIExtensions;
using System.Collections.Generic;
using System.Linq;
using HDS.App.Tests.Static;

namespace HDS.App.Tests.UnitTests {

    [TestClass]
    public class RestTests {

        #region Private
        private bool UserGenerator(out Aza aza) {
            var kunyeResponse = SDataEngine.GenerateDOEngine<Kunye>().InsertIfAbsentSync(
                new Kunye() {
                    IrtibatTelefonu = "5360331719",
                    Isim = "Hüseyin",
                    Memleket = "Sivas",
                    Meslek = EMeslek.Hekim,
                    Mevlid = DateTime.Parse("30.03.1981"),
                    Soyisim = "Sönmez"
                }, k => k.ID);
            aza = new Aza() { Sifre = "3132", TCKN = Convert.ToInt64($"25871077074") };
            if (kunyeResponse.Success) {
                var kunye = kunyeResponse.SingleResponse;
                aza.KunyeID = kunye.ID;
                var azaResponse = SDataEngine.GenerateDOEngine<Aza>().InsertIfAbsentSync(aza, a => a.TCKN);
                if (azaResponse.Success) {
                    aza = azaResponse.SingleResponse;
                    return true;
                }
            }
            return false;
        }

        private NameValueCollection Authenticate() {
            Assert.IsTrue(UserGenerator(out var aza));
            var session = SAPIValues.CurrentRemoteHost.RequestPost<Session, Session>(
                    new Session() { UserName = Convert.ToString(aza.TCKN), Password = aza.Sifre },
                    $"{SAPIValues.CurrentRPIKey}/{SAPIValues.CurrentRPIVersion}/aza/jeton").Result;
            Assert.IsTrue(session.ResponseObject?.Token.Success == true, session.Content);
            var response = SAPIValues.CurrentRemoteHost.RequestPost<Session, Session>(
                session.ResponseObject,
                $"{SAPIValues.CurrentRPIKey}/{SAPIValues.CurrentRPIVersion}/aza/oturum-ac").Result;
            Assert.IsTrue(response.ResponseObject?.Token.Success == true, response.ReasonPhrase);
            var token = response.ResponseObject.Token.AccessToken;
            Assert.AreNotEqual("", token);
            var coll = new NameValueCollection {
                { "X-HDS-Tahdit", token }
            };
            return coll;
        }

        #endregion

        [TestInitialize]
        public void BeforeTest() {
            Builder.Bootstrap();
        }

        [TestMethod]
        public async Task RESTAllColumnsFetched() {
            var headers = Authenticate();
            var result = await SAPIValues.CurrentRemoteHost.RequestRoute<IEnumerable<IDictionary<string, object>>>(
                GenericController.Routes[EApiRoutes.Columns], headers);
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Success, result.Content);
            Assert.AreNotEqual(0, result.ResponseObject.Count());
            result.ResponseObject.ToList().ForEach(col => Assert.IsTrue(col.Keys.Contains("name")));
        }

    }
}
