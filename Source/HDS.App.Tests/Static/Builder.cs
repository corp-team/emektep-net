﻿using HDS.App.Api.Static;
using HDS.App.Domain.Data;
using HDS.App.Domain.Enums;
using System;
using System.Web.Hosting;

namespace HDS.App.Tests.Static {
    public static class Builder {

        static Builder() {
            Bootstrap();
        }
        public static void Bootstrap() {
            SDbParams.SetConnectionString(DBConstants.LocalhostMySqlConnectionString, EConnectionStringMode.Debug, EServerType.MySql);
            SDbParams.SetConnectionString(DBConstants.RemoteMySqlConnectionString, EConnectionStringMode.Release, EServerType.MySql);
            SAPIValues.CurrentDebugHost = "http://localhost:166";
            SAPIValues.CurrentProductionHost = "http://localhost:166";
            SAPIValues.CurrentRPIVersion = "v1";
            SAPIValues.ConfigurationXMLPath = $@"{AppDomain.CurrentDomain.BaseDirectory}\Static\api.xml";
        }

        public static string CurrentConnectionString {
            get {
                return SDbParams.ConnectionString();
            }
        }

        public static string CurrentSchema {
            get {
                return SDbParams.LocalMachine ? DBConstants.LocalhostMySqlSchemaName : DBConstants.RemoteMySqlSchemaName;
            }
        }
    }
}
