﻿
using HDS.App.Extensions.Decorators;

namespace HDS.App.Tests.Models.Azalik {

    [ResourceLocator(typeof(Resources.Meslek))]
    public enum EMeslek {

        Tuccar = 1,
        Hekim,
        Alim,
        EvHanimi

    }

}
