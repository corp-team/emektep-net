﻿

using HDS.App.Domain.Objects;
using System;

namespace HDS.App.Tests.Models.Azalik {
    public class Kunye : EMBase<Kunye> {


        public string Isim { get; set; }

        public string Soyisim { get; set; }

        public string Memleket { get; set; }

        public string IrtibatTelefonu { get; set; }

        public DateTime Mevlid { get; set; }

        public EMeslek Meslek { get; set; }

        public override string TextValue => $"{Isim} {Soyisim}";


        protected override void EMMap(IDOTableBuilder<Kunye> builder) {
            builder.MapsTo(x => { x.SchemaName("AA").TableName("Kunyeler"); });

            builder.For(d => d.Isim).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(255);
            builder.For(d => d.Soyisim).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(64);
            builder.For(d => d.Memleket).IsTypeOf(EDataType.String).HasMaxLength(128);
            builder.For(d => d.IrtibatTelefonu).IsTypeOf(EDataType.String).HasMaxLength(32);
            builder.For(d => d.Mevlid).IsTypeOf(EDataType.Date);
            builder.For(d => d.Meslek).IsTypeOf(EDataType.Enum);

        }
    }
}
