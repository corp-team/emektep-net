﻿using HDS.App.Domain.Objects;

namespace HDS.App.Tests.Models.Azalik {
    public class Tahdit : EMBase<Tahdit> {


        public long AzaID { get; set; }

        public string Tarif { get; set; }

        public override string TextValue => Tarif;


        protected override void EMMap(IDOTableBuilder<Tahdit> builder) {
            builder.MapsTo(x => { x.SchemaName("AA").TableName("Tahditler"); });

            builder.For(d => d.Tarif).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(255);

            builder.ForeignKey(a => a.AzaID).References<Aza>(a => a.ID);
        }
    }
}
