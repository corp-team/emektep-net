﻿
using HDS.App.Domain.Objects;
using System;

namespace HDS.App.Tests.Models.Azalik {

    public class Aza : EMBase<Aza> {

        public long KunyeID { get; set; }

        public long TCKN { get; set; }

        public string Sifre { get; set; }

        public string AccessToken { get; set; }

        public string ActivationToken { get; set; }

        public string RecoveryToken { get; set; }

        public DateTime ActivationTokenExpiresOn { get; set; }

        public DateTime AccessTokenExpiresOn { get; set; }

        public DateTime RecoveryTokenExpiresOn { get; set; }

        public override string TextValue => TCKN.ToString();

        protected override void EMMap(IDOTableBuilder<Aza> builder) {

            builder.MapsTo(x => { x.SchemaName("AA").TableName("Azalar"); });

            builder.For(d => d.Sifre).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(64);
            builder.For(d => d.TCKN).IsTypeOf(EDataType.BigInt).IsRequired().HasMaxLength(11);

            builder.For(d => d.ActivationToken).IsTypeOf(EDataType.String).HasMaxLength(128);
            builder.For(d => d.AccessToken).IsTypeOf(EDataType.UniqueIdentifier);
            builder.For(d => d.RecoveryToken).IsTypeOf(EDataType.String).HasMaxLength(128);

            builder.For(d => d.ActivationTokenExpiresOn).IsTypeOf(EDataType.DateTime);
            builder.For(d => d.AccessTokenExpiresOn).IsTypeOf(EDataType.DateTime);
            builder.For(d => d.RecoveryTokenExpiresOn).IsTypeOf(EDataType.DateTime);

            builder.ForeignKey(d => d.KunyeID).References<Kunye>(d => d.ID);

            builder.UniqueKey(d => d.KunyeID);
            builder.UniqueKey(d => d.TCKN);
        }

    }

}
