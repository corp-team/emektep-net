﻿using ElMektep.Domain.Objects.Azalik;
using ElMektep.Domain.Views.Azalik;
using HDS.App.Api.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HDS.App.Extensions.Decorators;
using ElMektep.Alem.Decorators;
using System.Threading.Tasks;
using HDS.App.Engines;
using HDS.App.Api.Common;
using System.Web.Http.Cors;
using HDS.App.Extensions.Static;

namespace ElMektep.Alem.Controllers {

    [EnableCors(origins: "http://localhost:167", headers: "*", methods: "*", exposedHeaders: "X-HDS-Tahdit")]
    [Tahditli]
    public class AzaController : GenericController {

        public AzaController() : base(typeof(Aza), typeof(AzaView)) { }

        private Func<string, Session> FalseToken => (message) => {
            return new Session {
                Token = new ApiToken {
                    AccessToken = "",
                    Success = false
                },
                ClientMessage = message
            };
        };

        #region Session Actions
        [Route("jeton"), HttpPost, Musaadeli]
        public async Task<IHttpActionResult> Token([FromBody]Session session) {
            var eng = SDataEngine.GenerateDOEngine<Aza>();
            var existingResponse = (await eng.SelectSingle(a => a.TCKN == Convert.ToInt64(session.UserName) && a.Sifre == session.Password));
            if (existingResponse.Success) {
                var existing = existingResponse.SingleResponse;
                if (existing != null) {
                    if (DateTime.Now > existing.AccessTokenExpiresOn) {
                        session.Token.AccessToken = Guid.NewGuid().ToString("N");
                        session.Token.Success = true;
                        await eng.Update(existing.ID, (modifier) => modifier
                           .Update(a => a.AccessToken).Set(session.Token.AccessToken)
                           .Update(a => a.AccessTokenExpiresOn).Set(DateTime.Now.AddMinutes(11)));
                    } else {
                        session.Token.AccessToken = existing.AccessToken;
                        session.Token.Success = true;
                    }
                } else {
                    session = FalseToken("Invalid User Credentials");
                }
            } else {
                session = FalseToken(existingResponse.Message);
            }
            return Json(session);
        }
        [Route("oturum-ac"), HttpPost, Musaadeli]
        public async Task<IHttpActionResult> Logon([FromBody]Session session) {

            var eng = SDataEngine.GenerateDOEngine<Aza>();
            var token = session.Token.AccessToken;
            var existingResponse = (await eng.SelectSingle(a => a.AccessToken == token));
            if (existingResponse.Success) {
                var existing = existingResponse.SingleResponse;
                if (existing != null) {
                    if (DateTime.Now > existing.AccessTokenExpiresOn) {
                        session = FalseToken("Token Expired");
                    } else {
                        session.Token.AccessToken = existing.AccessToken;
                        session.Token.Success = true;
                    }
                } else {
                    session = FalseToken("Invalid User Credentials");
                }
            } else {
                session = FalseToken(existingResponse.Message);
            }
            return Json(session);

        }
        [Route("oturum-kapat"), HttpPost]
        public async Task<IHttpActionResult> Logoff([FromBody]Session session) {

            var eng = SDataEngine.GenerateDOEngine<Aza>();
            var token = session.Token.AccessToken;
            var existingResponse = (await eng.SelectSingle(a => a.AccessToken == token));
            if (existingResponse.Success) {
                var existing = existingResponse.SingleResponse;
                if (existing != null) {
                    session = FalseToken("Logout Success");
                } else {
                    session = FalseToken("Invalid User Credentials");
                }
            } else {
                session = FalseToken(existingResponse.Message);
            }
            return Json(session);

        }
        #endregion

    }
}
