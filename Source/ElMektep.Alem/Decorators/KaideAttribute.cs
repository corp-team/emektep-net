﻿using HDS.App.Api.Contracts;
using HDS.App.Domain.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace ElMektep.Alem.Decorators {
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = false, AllowMultiple = false)]
    sealed class KaideAttribute : FilterAttribute {

        private readonly List<string> _Kaideler;

        public KaideAttribute(params string[] kaideler) {
            _Kaideler = new List<string>(kaideler);
        }

        public string[] Kaideler => _Kaideler.ToArray();

    }
}