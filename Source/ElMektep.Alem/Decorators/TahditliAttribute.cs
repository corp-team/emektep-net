﻿using ElMektep.Alem.Code;
using ElMektep.Domain.Objects.Azalik;
using HDS.App.Api.Contracts;
using HDS.App.Domain.Contracts;
using HDS.App.Engines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace ElMektep.Alem.Decorators {
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = false, AllowMultiple = false)]
    sealed class TahditliAttribute : FilterAttribute, IAuthorizationFilter {

        private readonly static IDOEngine<Aza> _AzaEngine = SDataEngine.GenerateDOEngine<Aza>();

        public Task<HttpResponseMessage> ExecuteAuthorizationFilterAsync(HttpActionContext actionContext, CancellationToken cancellationToken, Func<Task<HttpResponseMessage>> continuation) {
            var controllerType = actionContext.ControllerContext.Controller.GetType();
            var actionName = actionContext.ActionDescriptor.ActionName;
            var kaideAttr = controllerType.GetMethod(actionName).GetCustomAttribute<KaideAttribute>();
            if (kaideAttr != null) {
                var tarifler = SDataEngine.GenerateDOEngine<Kaide>().SelectAllSync().CollectionResponse.Select(k => k.Tarif);
                if (!tarifler.Any(t => kaideAttr.Kaideler.Contains(t))) {
                    return Response(actionContext, "Rule Violation");
                }
            }
            if (controllerType.GetMethod(actionName).GetCustomAttribute<MusaadeliAttribute>() != null) {
                return continuation();
            } else if (actionContext.Request.Headers.TryGetValues("X-HDS-Tahdit", out var headers)) {
                if (!Celse.Cari(headers.FirstOrDefault(), out var aza)) {
                    return Response(actionContext, "Invalid Token");
                } else {
                    if (DateTime.Now > aza.AccessTokenExpiresOn) {
                        var token = Guid.NewGuid().ToString("N");
                        _AzaEngine.UpdateSync(aza.ID, (modifier) => modifier
                           .Update(a => a.AccessToken).Set(token)
                           .Update(a => a.AccessTokenExpiresOn).Set(DateTime.Now.AddMinutes(11)));
                    }
                    return continuation();
                }
            } else {
                return Response(actionContext, "No Token Provided");
            }

        }

        private Task<HttpResponseMessage> Response(HttpActionContext actionContext, string message) {
            actionContext.Response = new HttpResponseMessage {
                StatusCode = HttpStatusCode.Forbidden,
                RequestMessage = actionContext.ControllerContext.Request,
                ReasonPhrase = message
            };
            return Task.FromResult(actionContext.Response);
        }
    }
}