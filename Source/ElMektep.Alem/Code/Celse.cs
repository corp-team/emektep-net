﻿using ElMektep.Domain.Objects.Azalik;
using HDS.App.Engines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElMektep.Alem.Code {
    public static class Celse {

        public static bool Cari(string anahtar, out Aza aza) {
            var azaeng = SDataEngine.GenerateDOEngine<Aza>();
            aza = azaeng.SelectSingleSync(a => a.AccessToken == anahtar).SingleResponse;
            return aza != null;
        }

    }
}