﻿using ElMektep.Alem;
using HDS.App.Api.GenericRouting;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace ElMektep.Alem {
    public class WebApiApplication : System.Web.HttpApplication {

        protected void Application_Start() {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configure(DataConfig.Register);
            GlobalConfiguration.Configure(SApiInjector.InjectConfiguration);
        }


        protected void Application_BeginRequest() {

            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.GetCultureInfo("tr-TR");
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.GetCultureInfo("tr-TR");

        }

    }
}
