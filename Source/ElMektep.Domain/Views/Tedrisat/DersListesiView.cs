﻿using ElMektep.Domain.Objects.AkademikKadro;
using ElMektep.Domain.Objects.Kurum;
using ElMektep.Domain.Objects.Mufredat;
using ElMektep.Domain.Objects.Azalik;
using HDS.App.Domain.Aggregate;
using System;
using HDS.App.Extensions.Decorators;
using ElMektep.Domain.Objects.Giris;
using HDS.App.Extensions.Static;

namespace ElMektep.Domain.Views.Tedrisat {

    public class DersListesiView : AGBase<DersListesiView> {

        [IDPropertyLocator]
        public long DersYukuID { get; set; }

        public long DersID { get; set; }

        public long DonemID { get; set; }

        public long AzaID { get; set; }

        [TableColumn(Order = 1), FormField(EFormType.Insert | EFormType.Edit)]
        public string DersKodu { get; set; }

        [TableColumn(Order = 2), FormField(EFormType.Insert | EFormType.Edit)]
        public string DersAdi { get; set; }

        public int TeorikSaat { get; set; }
        [ComputedField(EComputedType.FormattedString, typeof(long))]
        [TableColumn(Order = 3), FormField(EFormType.Insert | EFormType.Edit)]
        public string TeorikSaatComputed {
            get {
                return Resources.DersListesiFormats.Saat.Puts(TeorikSaat);
            }
        }

        public int UygulamaSaat { get; set; }
        [ComputedField(EComputedType.FormattedString, typeof(long))]
        [TableColumn(Order = 4), FormField(EFormType.Insert | EFormType.Edit)]
        public string UygulamaSaatComputed {
            get {
                return Resources.DersListesiFormats.Saat.Puts(UygulamaSaat);
            }
        }


        public int Esdegerlik { get; set; }
        [ComputedField(EComputedType.FormattedString, typeof(long))]
        [TableColumn(Order = 5), FormField(EFormType.Insert | EFormType.Edit)]
        public string EsdegerlikComputed {
            get {
                return Resources.DersListesiFormats.Esdegerlik.Puts(Esdegerlik);
            }
        }

        public string OgretimUyesi { get; set; }

        protected override void Map(IAGViewBuilder<DersListesiView> builder) {

            builder
                .MapsTo(x => x.SchemaName("VW").ViewName("DersListesiView"))
                .Select<DersYuku>(li => li.Map(x => x.ID, x => x.DersYukuID))
                .InnerJoin<Program>().On<DersYuku>((prg, dy) => prg.ID == dy.ProgramID)
                .InnerJoin<OgretimUyesi>().On<DersYuku>((ogu, dy) => ogu.ID == dy.OgretimUyesiID)
                .InnerJoin<Aza>(li => li.Map(x => x.ID, x => x.AzaID)).On<OgretimUyesi>((uy, ogu) => uy.ID == ogu.AzaID)
                .InnerJoin<Kunye>(li => li.Map(x => x.Isim + " " + x.Soyisim, x => x.OgretimUyesi)).On<Aza>((a, b) => a.ID == b.KunyeID)
                .InnerJoin<Mufredat>().On<Program>((mf, prg) => mf.ProgramID == prg.ID)
                .InnerJoin<Donem>(li => li.Map(d => d.ID, x => x.DonemID))
                    .On<Mufredat>((dn, mf) => dn.MufredatID == mf.ID)
                .InnerJoin<Ders>(li =>
                    li.Map(d => d.DersKodu, x => x.DersKodu)
                    .Map(d => d.DersAdi, x => x.DersAdi)
                    .Map(d => d.TeorikSaat, x => x.TeorikSaat)
                    .Map(d => d.UygulamaSaat, x => x.UygulamaSaat)
                    .Map(d => d.Esdegerlik, x => x.Esdegerlik)
                    .Map(x => x.ID, x => x.DersID))
                .On<Donem>((drs, dn) => drs.DonemID == dn.ID);

        }

    }

}
