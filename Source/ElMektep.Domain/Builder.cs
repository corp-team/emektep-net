﻿using ElMektep.Domain.Resources;
using HDS.App.Api.Static;
using HDS.App.Domain.Data;
using HDS.App.Domain.Enums;
using HDS.App.Engines;
using System;
using System.Web.Hosting;

namespace ElMektep.Domain {
    public static class Builder {

        static Builder() {
            Bootstrap();
        }
        public static void Bootstrap() {
            SDbParams.SetConnectionString(DBConstants.LocalhostMySqlConnectionString, EConnectionStringMode.Debug, EServerType.MySql);
            SDbParams.SetConnectionString(DBConstants.RemoteMySqlConnectionString, EConnectionStringMode.Release, EServerType.MySql);
            SAPIValues.CurrentDebugHost      = "http://localhost/ElMektep.Alem";
            SAPIValues.CurrentProductionHost = "http://www.emektep.net";
            SAPIValues.CurrentRPIVersion     = "v1";
            SAPIValues.ConfigurationXMLPath  = HostingEnvironment.MapPath($"~/Static/api.xml"); ;
        }


        public static void RebuildDomain() {
            SDataEngine.WipeDomain(typeof(Builder).Assembly.GetName(), rebuild: true);
        }

        public static string CurrentConnectionString {
            get {
                return SDbParams.ConnectionString();
            }
        }

        public static string CurrentSchema {
            get {
                return SDbParams.LocalMachine ? DBConstants.LocalhostMySqlSchemaName : DBConstants.RemoteMySqlSchemaName;
            }
        }
    }
}
