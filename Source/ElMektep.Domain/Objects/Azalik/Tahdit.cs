﻿
using ElMektep.Domain.Objects.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Decorators;

namespace ElMektep.Domain.Objects.Azalik {
    public class Tahdit : EMBase<Tahdit> {


        public long AzaID { get; set; }

        public string Tarif { get; set; }

        public override string TextValue => Tarif;


        protected override void EMMap(IDOTableBuilder<Tahdit> builder) {
            builder.MapsTo(x => { x.SchemaName("AA").TableName("Tahditler"); });

            builder.For(d => d.Tarif).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(255);

            builder.ForeignKey(a => a.AzaID).References<Aza>(a => a.ID);
        }
    }
}
