﻿
using ElMektep.Domain.Objects.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Decorators;

namespace ElMektep.Domain.Objects.Azalik {
    public class Kaide : EMBase<Kaide> {


        public long TahditID { get; set; }

        public string Tarif { get; set; }

        public string MüsaadeliUnsur { get; set; }

        public override string TextValue => Tarif;

        protected override void EMMap(IDOTableBuilder<Kaide> builder) {
            builder.MapsTo(x => { x.SchemaName("AA").TableName("Kaideler"); });

            builder.For(d => d.Tarif).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(255);
            builder.For(d => d.MüsaadeliUnsur).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(512);

            builder.ForeignKey(a => a.TahditID).References<Aza>(a => a.ID);
        }
    }
}
