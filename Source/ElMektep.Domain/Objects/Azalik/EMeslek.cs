﻿
using HDS.App.Extensions.Decorators;

namespace ElMektep.Domain.Objects.Azalik {

    [ResourceLocator(typeof(Resources.Meslek))]
    public enum EMeslek {

        Tuccar = 1,
        Hekim,
        Alim,
        EvHanimi

    }

}
