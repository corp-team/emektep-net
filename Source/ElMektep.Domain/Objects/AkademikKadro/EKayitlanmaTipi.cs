﻿
using HDS.App.Extensions.Decorators;

namespace ElMektep.Domain.Objects.AkademikKadro {

    [ResourceLocator(typeof(Resources.KayitlanmaTipi))]
    public enum EKayitlanmaTipi {
        OSYM = 1,
        YatayGecis,
        EkKontenjan
    }

}