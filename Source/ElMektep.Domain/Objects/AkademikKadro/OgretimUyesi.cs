﻿using ElMektep.Domain.Objects.Core;
using ElMektep.Domain.Objects.Azalik;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Decorators;

namespace ElMektep.Domain.Objects.AkademikKadro {

    public class OgretimUyesi : EMBase<OgretimUyesi> {

        public string Unvani { get; set; }

        public long AzaID { get; set; }

        public override string TextValue => Unvani;

        protected override void EMMap(IDOTableBuilder<OgretimUyesi> builder) {

            builder.MapsTo(x => { x.SchemaName("AK").TableName("OgretimUyeleri"); });
            builder.For(d => d.Unvani).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(64);

            builder.ForeignKey(x => x.AzaID).References<Aza>(u => u.ID);
            builder.UniqueKey(x => x.AzaID);

        }

    }
}
