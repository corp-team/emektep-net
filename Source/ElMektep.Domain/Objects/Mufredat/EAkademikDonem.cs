﻿
using HDS.App.Extensions.Decorators;
using System.ComponentModel;

namespace ElMektep.Domain.Objects.Mufredat {

    [ResourceLocator(typeof(Resources.AkademikDonem))]
    public enum EAkademikDonem {

        BirinciYariyil = 1,
        IkinciYariyil,
        YazDonemi,
        Donem1,
        Donem2,
        Donem3,
        Donem4,
        Donem5,
        Donem6

    }

}