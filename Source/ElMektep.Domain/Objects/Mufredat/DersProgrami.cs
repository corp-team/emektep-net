﻿using ElMektep.Domain.Objects.AkademikKadro;
using ElMektep.Domain.Objects.Core;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Decorators;

namespace ElMektep.Domain.Objects.Mufredat {

    public class DersProgrami : EMBase<DersProgrami> {

        public string Baslik { get; set; }

        public long DersYukuID { get; set; }

        public long OgrenciID { get; set; }

        public override string TextValue => Baslik;

        protected override void EMMap(IDOTableBuilder<DersProgrami> builder) {

            builder.MapsTo(x => { x.SchemaName("MF").TableName("DersProgrami"); });
            builder.For(d => d.Baslik).IsTypeOf(EDataType.String).IsRequired();

            builder.ForeignKey(tc => tc.DersYukuID).References<DersYuku>(s => s.ID);
            builder.ForeignKey(tc => tc.OgrenciID).References<Ogrenci>(s => s.ID);

        }

    }
}
