﻿using ElMektep.Domain.Objects.Core;
using ElMektep.Domain.Objects.Kurum;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Decorators;
using HDS.App.Extensions.Static;
using System;

namespace ElMektep.Domain.Objects.Mufredat {

    public class AkademikTakvim : EMBase<AkademikTakvim> {

        public DateTime Baslangic { get; set; }

        public DateTime Bitis { get; set; }

        public string Aciklama { get; set; }

        public long? FakulteID { get; set; }

        public EAkademikDonem AkademikDonem { get; set; }

        public override string TextValue => Aciklama;

        protected override void EMMap(IDOTableBuilder<AkademikTakvim> builder) {

            builder.MapsTo(x => { x.SchemaName("MF").TableName("AkademikTakvim"); });
            builder.For(d => d.Baslangic).IsTypeOf(EDataType.Date).IsRequired();
            builder.For(d => d.Bitis).IsTypeOf(EDataType.Date);
            builder.For(d => d.Aciklama).IsTypeOf(EDataType.String).HasMaxLength(400).IsRequired();
            builder.For(d => d.AkademikDonem).IsTypeOf(EDataType.Enum).IsRequired();

            builder.ForeignKey(x => x.FakulteID).References<Fakulte>(x => x.ID);

        }

    }
}
