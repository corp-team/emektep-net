﻿using ElMektep.Domain.Objects.Core;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Decorators;

namespace ElMektep.Domain.Objects.Mufredat {

    public class Donem : EMBase<Donem> {

        public string DonemAdi { get; set; }

        public long MufredatID { get; set; }

        public override string TextValue => DonemAdi;

        protected override void EMMap(IDOTableBuilder<Donem> builder) {

            builder.MapsTo(x => { x.SchemaName("MF").TableName("Donemler"); });
            builder.For(d => d.DonemAdi).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(128);

            builder.ForeignKey(x => x.MufredatID).References<Mufredat>(x => x.ID);

        }

    }
}
