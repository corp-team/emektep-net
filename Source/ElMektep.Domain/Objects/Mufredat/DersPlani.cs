﻿using ElMektep.Domain.Objects.Core;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Decorators;
using System;

namespace ElMektep.Domain.Objects.Mufredat {

    public class DersPlani : EMBase<DersPlani> {

        public DateTime DersTarihi { get; set; }

        public long DersID { get; set; }

        public override string TextValue => DersID.ToString();

        protected override void EMMap(IDOTableBuilder<DersPlani> builder) {

            builder.MapsTo(x => { x.SchemaName("MF").TableName("DersPlani"); });
            builder.For(d => d.DersTarihi).IsTypeOf(EDataType.DateTime).IsRequired();

            builder.ForeignKey(tc => tc.DersID).References<Ders>(s => s.ID);

        }

    }
}
