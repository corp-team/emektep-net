﻿using ElMektep.Domain.Objects.Core;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Decorators;

namespace ElMektep.Domain.Objects.Mufredat {

    public class Ders : EMBase<Ders> {

        public string DersKodu { get; set; }

        public string DersAdi { get; set; }

        public int TeorikSaat { get; set; }

        public int UygulamaSaat { get; set; }

        public int Esdegerlik { get; set; }

        public long DonemID { get; set; }

        public override string TextValue => DersAdi;

        protected override void EMMap(IDOTableBuilder<Ders> builder) {

            builder.MapsTo(x => { x.SchemaName("MF").TableName("Dersler"); });
            builder.For(d => d.DersKodu).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(32);
            builder.For(d => d.DersAdi).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(128);
            builder.For(d => d.TeorikSaat).IsTypeOf(EDataType.Int).IsRequired();
            builder.For(d => d.UygulamaSaat).IsTypeOf(EDataType.Int).IsRequired();
            builder.For(d => d.Esdegerlik).IsTypeOf(EDataType.Int).IsRequired();

            builder.ForeignKey(x => x.DonemID).References<Donem>(x => x.ID);

        }

    }
}
