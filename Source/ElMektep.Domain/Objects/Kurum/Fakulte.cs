﻿using ElMektep.Domain.Objects.Core;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Decorators;

namespace ElMektep.Domain.Objects.Kurum {

    public class Fakulte : EMBase<Fakulte> {

        public string Adi { get; set; }

        public long UniversiteID { get; set; }

        public override string TextValue => Adi;

        protected override void EMMap(IDOTableBuilder<Fakulte> builder) {

            builder.MapsTo(x => { x.SchemaName("KR").TableName("Fakulteler"); });
            builder.For(d => d.Adi).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(128);

            builder.ForeignKey(x => x.UniversiteID).References<Universite>(x => x.ID);

        }

    }
}
