﻿using ElMektep.Domain.Objects.Core;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Decorators;

namespace ElMektep.Domain.Objects.Kurum {

    public class Program : EMBase<Program> {

        public string Adi { get; set; }

        public long MYOID { get; set; }

        public override string TextValue => Adi;

        protected override void EMMap(IDOTableBuilder<Program> builder) {

            builder.MapsTo(x => { x.SchemaName("KR").TableName("Programlar"); });
            builder.For(d => d.Adi).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(128);

            builder.ForeignKey(x => x.MYOID).References<MYO>(x => x.ID);

        }

    }
}
