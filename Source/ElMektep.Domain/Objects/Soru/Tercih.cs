﻿using ElMektep.Domain.Objects.Core;
using HDS.App.Domain.Objects;

namespace ElMektep.Domain.Objects.Soru {

    public class Tercih : EMBase<Tercih> {

        public string Ibare { get; set; }

        public bool Sahih { get; set; }

        public int Kademe { get; set; }

        public long SualID { get; set; }

        public override string TextValue => Ibare;

        protected override void EMMap(IDOTableBuilder<Tercih> builder) {

            builder.MapsTo(x => { x.SchemaName("IM").TableName("Tercihler"); });
            builder.For(d => d.Ibare).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(800);
            builder.For(d => d.Sahih).IsTypeOf(EDataType.Bool).IsRequired();
            builder.For(d => d.Kademe).IsTypeOf(EDataType.Int).IsRequired();

            builder.ForeignKey(d => d.SualID).References<Sual>(s => s.ID);

        }

    }
}
