﻿using ElMektep.Domain.Objects.Core;
using HDS.App.Domain.Objects;
using System;

namespace ElMektep.Domain.Objects.Soru {

    public class Sual : EMBase<Sual> {

        public Guid SeriNo { get; set; }

        public string Baslik { get; set; }

        public ESualTipi SualTipi { get; set; }

        public long ImtihanID { get; set; }

        public override string TextValue => Baslik;

        protected override void EMMap(IDOTableBuilder<Sual> builder) {

            builder.MapsTo(x => { x.SchemaName("IM").TableName("Sualler"); });

            builder.For(d => d.SeriNo).IsTypeOf(EDataType.UniqueIdentifier).IsAutoGenerated(() => Guid.NewGuid());
            builder.For(d => d.Baslik).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(800);
            builder.For(d => d.SualTipi).IsTypeOf(EDataType.Enum).IsRequired();

            builder.ForeignKey(x => x.ImtihanID).References<Imtihan>(x => x.ID);

        }

    }
}
