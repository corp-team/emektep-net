﻿using HDS.App.Extensions.Decorators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElMektep.Domain.Objects.Soru {

    [ResourceLocator(typeof(Resources.SualTipi))]
    public enum ESualTipi {

        Secmeli = 1,
        DogruYanlis = 2,
        Klasik = 3

    }

}
