﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ElMektep.Alem;
using ElMektep.Alem.Controllers;
using System.Web.Http;
using ElMektep.Alem.Tests.Controllers.Generic;
using ElMektep.Domain.Objects.Azalik;
using ElMektep.Domain.Views.Azalik;
using System;
using HDS.App.Engines;
using HDS.App.Api.Common;
using System.Threading.Tasks;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Static;
using HDS.App.Api.Static;
using HDS.App.Api.APIExtensions;
using System.Collections.Specialized;

namespace ElMektep.Alem.Tests.Controllers {
    [TestClass]
    public class AzaRESTApiTest : GenericApiTest<AzaController, Aza, AzaView> {

        private Kunye _Kunye;

        protected override AzaView EntityModifier(Aza domainObject) {
            return new AzaView() {
                AzaID = domainObject.ID,
                AzaSifre = domainObject.Sifre,
                AzaTCKN = domainObject.TCKN,
                AzaKunyeID = _Kunye?.ID ?? 0,
                KunyeIrtibatTelefonu = _Kunye?.IrtibatTelefonu,
                KunyeIsim = _Kunye?.Isim,
                KunyeSoyisim = _Kunye?.Soyisim,
                KunyeMemleket = _Kunye?.Memleket,
                KunyeMeslek = _Kunye?.Meslek ?? EMeslek.Alim,
                KunyeMevlid = _Kunye?.Mevlid ?? DateTime.MinValue
            };
        }

        protected override void TemplateEntityGenerator(out AzaView domainObject) {
            var dml = SDataEngine.GenerateDOEngine<Kunye>().InsertSync(new Kunye {
                IrtibatTelefonu = $"5360331{3.RandomDigits()}",
                Isim = 1.RandomWords(),
                Memleket = 1.RandomWords(),
                Meslek = EMeslek.Hekim,
                Mevlid = DateTime.Parse($"30.03.19{2.RandomDigits()}"),
                Soyisim = 1.RandomWords(),
            });
            if (!dml.Success) {
                throw dml.Fault;
            }
            _Kunye = dml.SingleResponse;
            domainObject = new AzaView() {
                AzaSifre = "3132",
                AzaTCKN = Convert.ToInt64($"35871{6.RandomDigits()}"),
                KunyeIrtibatTelefonu = _Kunye.IrtibatTelefonu,
                KunyeIsim = _Kunye.Isim,
                KunyeMemleket = _Kunye.Memleket,
                KunyeMeslek = _Kunye.Meslek,
                KunyeMevlid = _Kunye.Mevlid,
                KunyeSoyisim = _Kunye.Soyisim,
                AzaKunyeID = _Kunye.ID
            };
        }

        [TestMethod]
        public async Task TokenFetched() {
            Assert.IsTrue(UserGenerator(out var aza));
            var session = await ParseResponse<Session>(await ApiController.Token(new Session() { UserName = Convert.ToString(aza.TCKN), Password = aza.Sifre }));
            Assert.IsTrue(session.Token.Success);
            Assert.AreNotEqual("", session.Token.AccessToken);
        }

        [TestMethod]
        public async Task LoggedOn() {
            Assert.IsTrue(UserGenerator(out var aza));
            var session = await ParseResponse<Session>(await ApiController.Token(new Session() { UserName = Convert.ToString(aza.TCKN), Password = aza.Sifre }));
            var response = await ParseResponse<Session>(await ApiController.Logon(session));
            Assert.IsTrue(response.Token.Success, response.ClientMessage);
            Assert.AreNotEqual("", response.Token.AccessToken);
        }

        [TestMethod]
        public async Task LoggedOff() {
            Assert.IsTrue(UserGenerator(out var aza));
            var session = await ParseResponse<Session>(await ApiController.Token(new Session() { UserName = Convert.ToString(aza.TCKN), Password = aza.Sifre }));
            var response = await ParseResponse<Session>(await ApiController.Logoff(session));
            Assert.IsFalse(response.Token.Success, response.ClientMessage);
            Assert.AreEqual("", response.Token.AccessToken);
        }

        private bool UserGenerator(out Aza aza) {
            var kunye = new Kunye() { IrtibatTelefonu = "5360331719", Isim = "Hüseyin", Memleket = "Sivas", Meslek = EMeslek.Hekim, Mevlid = DateTime.Parse("30.03.1981"), Soyisim = "Sönmez" };
            var kunyeResponse = SDataEngine.GenerateDOEngine<Kunye>().InsertIfAbsentSync(kunye, k => k.ID);
            aza = new Aza() { Sifre = "3132", TCKN = Convert.ToInt64($"25871077074") };
            if (kunyeResponse.Success) {
                _Kunye = kunyeResponse.SingleResponse;
                aza.KunyeID = _Kunye.ID;
                var azaResponse = SDataEngine.GenerateDOEngine<Aza>().InsertIfAbsentSync(aza, a => a.TCKN);
                if (azaResponse.Success) {
                    aza = azaResponse.SingleResponse;
                    return true;
                }
            }
            return false;
        }

        protected override NameValueCollection Authenticate() {
            Assert.IsTrue(UserGenerator(out var aza));
            var session = SAPIValues.CurrentRemoteHost.RequestPost<Session, Session>(
                    new Session() { UserName = Convert.ToString(aza.TCKN), Password = aza.Sifre },
                    $"{SAPIValues.CurrentRPIKey}/{SAPIValues.CurrentRPIVersion}/aza/jeton").Result;
            Assert.IsTrue(session.ResponseObject?.Token.Success == true, session.Content);
            var response = SAPIValues.CurrentRemoteHost.RequestPost<Session, Session>(
                session.ResponseObject,
                $"{SAPIValues.CurrentRPIKey}/{SAPIValues.CurrentRPIVersion}/aza/oturum-ac").Result;
            Assert.IsTrue(response.ResponseObject?.Token.Success == true, response.ReasonPhrase);
            var token = response.ResponseObject.Token.AccessToken;
            Assert.AreNotEqual("", token);
            var coll = new NameValueCollection {
                { "X-HDS-Tahdit", token }
            };
            return coll;
        }
    }
}
