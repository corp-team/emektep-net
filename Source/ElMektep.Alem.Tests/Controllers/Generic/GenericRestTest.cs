﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HDS.App.Domain.Objects;
using HDS.App.Api.Contracts;
using System.Threading.Tasks;
using HDS.App.Domain.Aggregate;
using System.Collections.Generic;
using System.Linq;
using HDS.App.Extensions.Decorators;
using Newtonsoft.Json;
using System.Web.Http;
using System.Threading;
using System.Net.Http;
using HDS.App.Domain.Contracts;
using System.IO;
using ElMektep.Domain;
using HDS.App.Api.APIExtensions;
using HDS.App.Api.Static;
using HDS.App.Api.Common;
using System.Collections.Specialized;

namespace ElMektep.Alem.Tests.Controllers.Generic {

    [TestClass]
    public abstract class GenericRestTest<TController, TDomain, TAggregate>
        where TDomain : DOBase<TDomain>
        where TAggregate : AGBase<TAggregate>
        where TController : GenericController {

        protected readonly TController ApiController = Activator.CreateInstance<TController>();

        protected abstract void TemplateEntityGenerator(out TAggregate domainObject);

        protected abstract TAggregate EntityModifier(TDomain domainObject);

        protected abstract NameValueCollection Authenticate();
        private NameValueCollection _Headers;

        public void BeforeTestREST() {
            Builder.Bootstrap();
            _Headers = Authenticate();
        }

        [TestMethod]
        public async Task RESTAllRecordsFetched() {
            BeforeTestREST();
            var inserteds = new List<TDomain>();
            for (int i = 0; i < 10; i++) {
                TemplateEntityGenerator(out var templateEntity);
                var resp = await SAPIValues.CurrentRemoteHost.RequestPost<DMLResponse<TDomain>, TAggregate>(
                    templateEntity, GenericController.Routes[EApiRoutes.Insert], _Headers);
                inserteds.Add(resp.ResponseObject.SingleResponse);
            }
            var result = await SAPIValues.CurrentRemoteHost.RequestRoute<IEnumerable<TAggregate>>(GenericController.Routes[EApiRoutes.All], _Headers);
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Success, result.ReasonPhrase);
            Assert.AreNotEqual(0, result.ResponseObject.Count());
            result.ResponseObject.ToList().ForEach(d => Assert.IsTrue(d.IDProperty != 0));
            inserteds.ForEach(async ins => {
                var deleted = await SAPIValues.CurrentRemoteHost.RequestDelete($"{GenericController.Routes[EApiRoutes.Delete]}/{ins.ID}", _Headers);
                Assert.IsTrue(deleted.Success);
            });
        }

        [TestMethod]
        public async Task RESTOneFetched() {
            BeforeTestREST();
            TemplateEntityGenerator(out var one);
            var resp = await SAPIValues.CurrentRemoteHost.RequestPost<DMLResponse<TDomain>, TAggregate>(
                    one, GenericController.Routes[EApiRoutes.Insert], _Headers);
            Assert.IsTrue(resp.Success, resp.ReasonPhrase);
            var inserted = resp.ResponseObject.SingleResponse;
            var fetchedResponse = await SAPIValues.CurrentRemoteHost.RequestRoute<TAggregate>($"{GenericController.Routes[EApiRoutes.One]}/{inserted.ID}", _Headers);
            Assert.IsNotNull(fetchedResponse);
            Assert.IsTrue(fetchedResponse.Success, fetchedResponse.ReasonPhrase);
            Assert.AreNotEqual(0, fetchedResponse.ResponseObject.IDProperty);
        }

        [TestMethod]
        public async Task RESTOneRecordCRUD() {
            BeforeTestREST();
            TemplateEntityGenerator(out var templateEntity);
            var resp = await SAPIValues.CurrentRemoteHost.RequestPost<DMLResponse<TDomain>, TAggregate>(
                templateEntity, GenericController.Routes[EApiRoutes.Insert], _Headers);
            var inserted = resp.ResponseObject.SingleResponse;
            Assert.IsNotNull(inserted);
            Assert.AreNotEqual(0, inserted.ID);
            var modifiedResponse = await SAPIValues.CurrentRemoteHost.RequestPost<DMLResponse<TDomain>, TAggregate>(
                EntityModifier(inserted), $"{GenericController.Routes[EApiRoutes.Edit]}/{inserted.ID}", _Headers);
            Assert.IsNotNull(modifiedResponse);
            Assert.IsTrue(modifiedResponse.Success);
            var modified = modifiedResponse.ResponseObject.SingleResponse;
            Assert.IsNotNull(modified);
            Assert.AreEqual(inserted.ID, modified.ID, modifiedResponse.ReasonPhrase);
            var deleted = await SAPIValues.CurrentRemoteHost.RequestDelete($"{GenericController.Routes[EApiRoutes.Delete]}/{modified.ID}", _Headers);
            Assert.IsTrue(deleted.Success, deleted.ReasonPhrase);
        }

        [TestMethod]
        public async Task RESTAllColumnsFetched() {
            BeforeTestREST();
            var result = await SAPIValues.CurrentRemoteHost.RequestRoute<IEnumerable<IDictionary<string, object>>>(GenericController.Routes[EApiRoutes.Columns], _Headers);
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Success, result.ReasonPhrase);
            Assert.AreNotEqual(0, result.ResponseObject.Count());
            result.ResponseObject.ToList().ForEach(col => Assert.IsTrue(col.Keys.Contains("name")));
        }

        [TestMethod]
        public async Task RESTAllFieldsFetched() {
            BeforeTestREST();
            var result = await SAPIValues.CurrentRemoteHost.RequestRoute<IEnumerable<IDictionary<string, object>>>(
                $"{GenericController.Routes[EApiRoutes.Fields]}/{(int)EFormType.Insert}", _Headers);
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Success, result.ReasonPhrase);
            Assert.AreNotEqual(0, result.ResponseObject.Count());
            result.ResponseObject.ToList().ForEach(col => Assert.IsTrue(col.Keys.Contains("name")));
        }

    }
}
