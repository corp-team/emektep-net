if ($.fn.dataTableExt) {
    $.extend($.fn.dataTableExt.oSort, {
        "alt-string-pre": function (a) {
            return /alt="(.*?)"/.test(a) ? a.match(/alt="(.*?)"/)[1].toLowerCase() : '';
        },

        "alt-string-asc": function (a, b) {
            return ((a < b) ? -1 : ((a > b) ? 1 : 0));
        },

        "alt-string-desc": function (a, b) {
            return ((a < b) ? 1 : ((a > b) ? -1 : 0));
        }
    });
}
$.fn.extend({
    setUpload: function (elementid) {
        var onfilechange = function () {
            var reader = new FileReader();
            reader.addEventListener("load", function () {
                document.getElementById(elementid).value = reader.result;
            }, false);
            if ($(this)[0].files[0]) {
                reader.readAsDataURL($(this)[0].files[0]);
            }
        }
        $(this).change(onfilechange);
    },
    hasAttr: function (attrname) {
        var attr = $(this).attr(attrname);
        return typeof attr !== typeof undefined && attr !== false;
    },
    freeze: function (callback) {
        var frm = $('#' + $(this).attr('id'));
        $(frm).find(':input').attr('disabled', true);
        $(frm).find('select').attr('disabled', true);
        $(frm).find('[data-dismiss="modal"]').removeAttr('disabled');
        callback && callback($(frm));
    },
    unfreeze: function (callback) {
        var frm = $('#' + $(this).attr('id'));
        $(frm).find(':input:disabled').removeAttr('disabled');
        $(frm).find('select').removeAttr('disabled');
        callback && callback($(frm));
    },
    screen: function (parentContainer, on) {
        if ($('#__screen_div').length > 0) {
            $('#__screen_div').remove();
        }
        if (on !== false) {
            $(this).append($('<div id="__screen_div"/>'));
            $('#__screen_div')
                .css({ position: 'absolute', top: ($(this).offset().top - parentContainer.offset().top) + 'px', left: '0px', 'z-index': 9990, 'bg-color': '#FFF', opacity: '0.5%' })
                .width($(this).width()).height($(this).height());
        }
    },
    loading: function (callback) {
        var self = $(this);
        $(self).screen($('body'), true);
        $('#__screen_div').append($('<img id="__hex_loader" alt="__hex_loader" src="/images/icons/hex-loader.gif"/>'));
        $('#__hex_loader').css({ position: 'absolute', top: '15%', left: '34%', 'z-index': 9991, opacity: 0.3 });
        callback(function () {
            $(self).screen($('body'), false);
        });

    },
    unloading: function () {
        $(self).screen($('body'), false);
    }
});

Object.defineProperty(Object.prototype, "getKeys", {
    value: function () {
        var r = [];
        for (var k in this) {
            if (!this.hasOwnProperty(k))
                continue;
            r.push(k);
        }
        return r;
    },
    enumerable: false
});
Array.prototype.extend = function (arr) {
    arr.forEach(function (a) { this.push(a) }, this);
    return this;
}

