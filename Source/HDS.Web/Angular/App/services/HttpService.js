﻿App.service('HttpService', function ($http, $log, $rootScope) {
    var service = this;
    service.getRequest = function (route, version) {
        return {
            then: function (callback) {
                $http.get('/Angular/Config/ApiConfig.json').success(function (apiConfig) {
                    var host = apiConfig.Hosts.CurrentDebugHost;
                    service.tokenize(host, function (session) {
                        $http.get(host + (version || "v1") + '/' + route, { headers: { "X-HDS-Tahdit": session.Token.AccessToken } })
                            .success(function (data) {
                                callback(data);
                            })
                            .error(function (data, code) {
                                callback(data);
                                $log.error(data, code);
                            });
                    }, service.session);
                });
            }
        };
    };
    service.postRequest = function (route, model, version) {
        return {
            then: function (callback) {
                $http.get('/Angular/Config/ApiConfig.json').success(function (apiConfig) {
                    var host = apiConfig.Hosts.CurrentDebugHost;
                    service.tokenize(host, function (session) {
                        $http.post(host + (version || "v1") + '/' + route, model, { headers: { "X-HDS-Tahdit": session.Token.AccessToken } })
                            .success(function (data) {
                                callback(data);
                            })
                            .error(function (data, code) {
                                callback(data);
                                $log.error(data, code);
                            });
                    }, service.session);
                });
            }
        };
    };
    service.deleteRequest = function (route, version) {
        return {
            then: function (callback) {
                $http.get('/Angular/Config/ApiConfig.json').success(function (apiConfig) {
                    var host = apiConfig.Hosts.CurrentDebugHost;
                    service.tokenize(host, function (session) {
                        $http.delete(host + (version || "v1") + '/' + route, { headers: { "X-HDS-Tahdit": session.Token.AccessToken } })
                            .success(function (data) {
                                callback(data);
                            })
                            .error(function (data, code) {
                                callback(data);
                                $log.error(data, code);
                            });
                    }, service.session);
                });
            }
        };
    };
    service.tokenize = function (host, callback, session, version) {
        $http.post(host + (version || "v1") + '/azalar/jeton', session || { UserName: "25871077074", Password: "3132" })
            .success(function (data) {
                service.session = data;
                callback(service.session);
            })
            .error(function (data, code) {
                service.session = data;
                callback(service.session);
                $log.error(msg, code);
            });
    };
});