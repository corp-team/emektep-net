﻿App.service('QService', ['$state', 'jQueryService', function ($state, jqs) {
    var service = this;
    service.setScope = function (scope) {
        service.scope = scope;
    };
    service.injectReadyHandler = function (callback) {
        $(function () {
            callback(jQuery);
        });
    };
    service.announceDone = function (message, doneback) {
        $("#modal-announce").announce(message, true, function () {
            doneback && doneback();
            $state.go('items-table', { domain: service.scope.domain, command: 'list-items' });
        });
    };
    service.announceFailed = function (message, doneback) {
        $("#modal-announce").announce(message, false, doneback);
    };
    service.announceDoneStatic = function (message, doneback) {
        $("#modal-announce").announce(message, true, doneback);
    };
    service.ask = function (message, approved, cancelled) {
        $("#modal-ask").ask(message, approved, cancelled);
    };
}]);