﻿App.directive("request", ['HttpService', function (hs) {
    return {
        restrict: 'A',
        link: function ($scope, $element, $attrs) {
            hs.getRequest($attrs.spRoute).then(function (items) {
                $scope[$attrs.spAs] = items;
                $scope.$emit('GET:' + $attrs.spKey + ':OK', items );
            });
        }
    };
}]);