﻿App.controller('InsertItemFrameController', ['$scope', '$state', '$stateParams', 'HttpService', 'BusService', 'QService', function ($scope, $state, $stateParams, hs, bus, qs) {

    $scope.domain = $stateParams.domain;
    $scope.state = $state.current;
    $scope.params = $stateParams;
    $scope.command = $stateParams.command;

    qs.setScope($scope);

    bus.onPostReceived($scope, $scope.domain + ':insert', function (data) {
        qs.announceDone("Ekleme Başarılı");
    }, function (data) {
        qs.announceFailed(data.Message);
    });

    bus.onGetReceived($scope, $scope.domain + ':translations', function (data) {
        $('#insert-form-container').unloading();
    });

    bus.onGetReceived($scope, $scope.domain, function (data) {
        $('#insert-form-container').unloading();
    });

    bus.onGetReceived($scope, 'insertframe:fields', function (data) {
        $scope.fields = data;
        $scope.model = {};
        setTimeout(function () {
            $('.dtpicker-applied').datetimepicker({
                locale: 'tr'
            }).on("dp.change", function (event) {
                console.log(event.target);
                var fieldName = event.target.id;
                $scope.model[fieldName] = event.date.format("YYYY-MM-DD HH:mm:ss")
            });
            if (/insert-.+/.test($scope.command)) {
                console.log($scope.command);
            } else {
                $('.select2-plain').each(function (i, el) {
                    if ($(el).attr('data-url')) {
                        $(el).select2({
                            ajax: {
                                url: $(el).attr('data-url')
                            }
                        });
                    } else {
                        $(el).change(function (event) {
                            var e = $(event.target);
                            var fieldName = $(e).attr("id");
                            $scope.model[fieldName] = $(e).val();
                        });
                        $(el).select2({
                            placeholder: $(el).attr('data-placeholder')
                        });
                    }
                });
            }
            $('.select2-multiple').select2({ multiple: true });
        }, 20);

    });

    function init() {
        $('#insert-form-container').loading(function (lastback) {
            hs.getRequest($scope.domain + "/translations").then(function (lexicon) {
                $scope.lexicon = lexicon;
                lastback();
            });
        });
    }

    $scope.submitInsert = function () {
        var valid = true;
        for (var i = 0; i < $scope.fields.length; i++) {
            var field = $scope.fields[i];
            var id = field.name;
            if (field.isRequired) {
                var inp = $('#' + id);
                if (!$(inp).val()) {
                    $(inp).css({ border: '1px solid red' });
                    valid = false;
                } else {
                    $(inp).css({ border: '1px solid green' });
                }
            }
        } if (valid) {
            hs.postRequest($scope.domain + "/insert", $scope.model).then(function (data) {
                bus.emitPostRequest($scope, $scope.domain + ':insert', data, data.Success);
            });
        }
    };

    init();

}]);