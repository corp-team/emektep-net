﻿App.controller('ItemsTableController', ['$scope', '$state', '$stateParams', 'HttpService', 'BusService', 'QService', function ($scope, $state, $stateParams, hs, bus, qs) {

    $scope.domain = $stateParams.domain;
    $scope.command = $stateParams.command;

    $scope.state = $state.current;
    $scope.params = $stateParams;

    qs.setScope($scope);

    var dtable;

    bus.onDeleteReceived($scope, $scope.domain, function (data) {
        qs.announceDoneStatic("Silme Başarılı");
        $('#items-table').dataTable().fnDestroy();
        init();
    }, function (data) {
        qs.announceFailed(data.Message);
    });

    function init() {
        $('#items-container').loading(function (lastback) {
            hs.getRequest($scope.domain + "/translations").then(function (lexicon) {
                $scope.lexicon = lexicon;
                hs.getRequest($scope.domain + "/columns").then(function (columns) {
                    hs.getRequest($scope.domain + "/all").then(function (data) {
                        tablegenerator(data.CollectionResponse, columns);
                        bus.emitGetRequest($scope, $scope.domain + ':allpluscols', { rows: data.CollectionResponse, columns: columns }, data.Success);
                        lastback();
                    });
                });
            });
        });
    }

    function tablegenerator(rows, columns) {
        if (!$.fn.DataTable.isDataTable('#items-table')) {
            dtable = $('#items-table').DataTable({
                dom: 'T<"clear">lfrtip',
                tableTools: {
                    "sRowSelect": "single"
                },
                "data": rows,
                "columns":
                columns.map(function (col) { return { title: col.label, data: col.name }; }).extend([{ title: "Komutlar", data: "" }]),
                "columnDefs": [{
                    "targets": -1,
                    "data": null,
                    "width": "9%",
                    "defaultContent": '<center>' +
                        '<button type="button" title="Görüntüle" class="display btn btn-warning btn-xs"><i class="fa fa-search-plus"></i></button>&nbsp;' +
                        '<button type="button" title="Düzenle" class="edit btn btn-success btn-xs"><i class="fa fa-edit"></i></button>&nbsp;' +
                        '<button type="button" title="Sil" class="remove btn btn-danger btn-xs"><i class="fa fa-times"></i></button>&nbsp;' +
                        '</center>'
                }, {
                    "targets": 0,
                    "visible": true
                }],
                "drawCallback": function (settings) {
                    //$scope.$$tablegenerator(rows, columns);
                },
                "processing": true,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Turkish.json"
                }
            });
        }
        $('#items-table tbody').unbind('click').on('click', 'button', function () {
            var button = $(this);
            if ($(button).hasClass('display')) {
                setTimeout(function () {
                    var data = dtable.row($(button).parents('tr')).data();
                    $state.go('edit-form', { domain: $scope.domain, id: data.IDProperty, command: 'display-item' });
                }, 200);
            }
            if ($(button).hasClass('edit')) {
                setTimeout(function () {
                    var data = dtable.row($(button).parents('tr')).data();
                    $state.go('edit-form', { domain: $scope.domain, id: data.IDProperty, command: 'edit-item' });
                }, 50);
            }
            if ($(button).hasClass('remove')) {
                qs.ask($scope.lexicon.TitleEntity + ' Silinecek', function () {
                    setTimeout(function () {
                        var data = dtable.row($(button).parents('tr')).data();
                        hs.deleteRequest($scope.domain + "/delete/" + data.IDProperty).then(function (data) {
                            bus.emitDeleteRequest($scope, $scope.domain, data, data.Success);
                        });
                    }, 50);
                });
            }
        });
        if ($('#demo-checkbox-radio').length <= 0) {
            $('input[type="checkbox"]:not(".switch")').iCheck({
                checkboxClass: 'icheckbox_minimal-grey',
                increaseArea: '20%' // optional
            });

            $('input[type="radio"]:not(".switch")').iCheck({
                radioClass: 'iradio_minimal-grey',
                increaseArea: '20%' // optional
            });
        }

        //BEGIN CHECKBOX TABLE
        $('.checkall').on('ifChecked ifUnchecked', function (event) {
            if (event.type === 'ifChecked') {
                $(this).closest('table').find('input[type=checkbox]').iCheck('check');
            } else {
                $(this).closest('table').find('input[type=checkbox]').iCheck('uncheck');
            }
        });
    }

    init();
}]);