﻿App.factory('MenuItemsFactory', function () {
    return function () {
        return [
            { title: "Giriş Sayfası", route: "index", iconClass: "fa-tachometer" },
            {
                title: "Azalık", route: "#", iconClass: "fa-tachometer", submenu: [
                    { title: "Azalar Listesi", route: "items-table({ domain: 'azalar' })", iconClass: "fa-user" }
                    //{ title: "İş Emri Düzenle", route: "edit-form({ domain: 'serviceorders', id: 1 })", iconClass: "fa-pencil-square-o" }
                ]
            }
        ];
    };
});