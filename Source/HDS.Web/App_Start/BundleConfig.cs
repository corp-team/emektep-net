﻿using System.Web.Optimization;

namespace HDS.Web {
    public class BundleConfig {

        public static void RegisterBundles(BundleCollection bundles) {
            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.validate*"));

            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js",
            //          "~/Scripts/respond.js"));

            //bundles.Add(new StyleBundle("~/Content/css").Include(
            //          "~/Content/bootstrap.css",
            //          "~/Content/site.css"));
        }

        internal static void RegisterDashboardBundles(BundleCollection bundles) {

            bundles.Add(
                new StyleBundle("~/Dashboard/Core/css").Include(
                    "~/Content/site.css"
                )
            );
            bundles.Add(
                new StyleBundle("~/Dashboard/Vendors/css").Include(
                    "~/vendors/jquery-ui-1.10.4.custom/css/ui-lightness/jquery-ui-1.10.4.custom.min.css",
                    "~/vendors/font-awesome/css/font-awesome.min.css",
                    //"~/vendors/bootstrap/css/bootstrap.min.css",
                    "~/Content/bootstrap.css",
                    "~/vendors/animate.css/animate.css",
                    "~/vendors/jquery-pace/pace.css",
                    "~/vendors/iCheck/skins/all.css",
                    //"~/vendors/vendors/jquery-notific8/jquery.notific8.min.css",
                    "~/vendors/vendors/vendors/bootstrap-daterangepicker/daterangepicker-bs3.css",
                    "~/vendors/vendors/vendors/calendar/zabuto_calendar.min.css"
                )
            );

            bundles.Add(
                new StyleBundle("~/Dashboard/Themes/css").Include(
                    "~/css/style-responsive.css"
                )
            );

            bundles.Add(
                new ScriptBundle("~/Dashboard/Global/scripts").Include(
                    "~/Scripts/jquery-{version}.js",
                    "~/vendors/jquery-ui.js",
                    "~/Scripts/bootstrap.js",
                    //"~/vendors/bootstrap/js/bootstrap.min.js",
                    "~/vendors/bootstrap-hover-dropdown/bootstrap-hover-dropdown.js",
                    "~/vendors/metisMenu/jquery.metisMenu.js",
                    "~/vendors/slimScroll/jquery.slimscroll.js",
                    "~/vendors/jquery-cookie/jquery.cookie.js",
                    "~/vendors/iCheck/icheck.min.js",
                    "~/vendors/iCheck/custom.min.js",
                    //"~/vendors/jquery-notific8/jquery.notific8.min.js",
                    "~/vendors/holder/holder.js",
                    "~/vendors/responsive-tabs/responsive-tabs.js",
                    "~/vendors/jquery-news-ticker/jquery.newsTicker.min.js",
                    "~/vendors/moment/moment-with-locales.js",
                    "~/vendors/flot-chart/jquery.flot.js",
                    "~/vendors/DataTables/media/js/jquery.dataTables.js",
                    "~/vendors/extensions.js"
                )
            );
            bundles.Add(
                new ScriptBundle("~/Dashboard/App/scripts").Include(
                    "~/vendors/angular/angular.min.js",
                    "~/vendors/angular/angular-route.min.js",
                    "~/vendors/angular/angular-ui-router.js",
                    "~/vendors/angular/ocLazyLoad.js",
                    "~/vendors/bootstrap/ui-bootstrap.js",

                    "~/Angular/App/app.js",
                    "~/Angular/App/directives/*.js",
                    "~/Angular/App/services/*.js",
                    "~/Angular/App/factories/*.js",
                    "~/Angular/App/filters/*.js",
                    "~/Angular/App/controllers/*.js",
                    "~/Angular/App/generic-controllers/*.js",
                    "~/Angular/App/components/*.js"
                )
            );
        }

    }
}
