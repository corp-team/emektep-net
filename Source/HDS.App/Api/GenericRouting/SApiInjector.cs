﻿using HDS.App.Api.Common;
using HDS.App.Api.Static;
using System;
using System.Web.Http;

namespace HDS.App.Api.GenericRouting {
    public static class SApiInjector {

        public static void InjectConfiguration(HttpConfiguration config) {
            var apiConfig = SAPIValues.LoadConfiguration();
            foreach (var route in apiConfig.Routes) {
                foreach (var method in route.Methods) {
                    config.Routes.MapHttpRoute(
                    name: $"ApiRoute-{Guid.NewGuid().ToString("N")}",
                    routeTemplate: $"{SAPIValues.CurrentRPIKey}/{SAPIValues.CurrentRPIVersion}/{route.Key}/{{action}}/{{argument}}",
                    defaults: new { action = method.Action, argument = RouteParameter.Optional },
                    handler: new HQHandler(config, route),
                    constraints: null
                );
                }
            }
        }

    }
}
