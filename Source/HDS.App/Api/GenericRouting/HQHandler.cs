﻿using HDS.App.Api.Common;
using HDS.App.Api.Static;
using HDS.App.Extensions.Static;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dispatcher;

namespace HDS.App.Api.GenericRouting {

    public class HQHandler : DelegatingHandler {

        private Route route;

        public HQHandler(HttpConfiguration httpConfiguration, Route route) {
            this.route = route;
            InnerHandler = new HttpControllerDispatcher(httpConfiguration);
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken) {
            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(await ExecuteAction(request));
            return await tsc.Task;
        }

        private async Task<HttpResponseMessage> ExecuteAction(HttpRequestMessage request) {
            var action = request.GetRequestContext().RouteData.Values["action"].ToString();
            var method = this.route.Methods.FirstOrDefault(m => m.Action == action);
            if (method == null) {
                method = this.route.Methods.First(m => m.Action.IndexOf('/') > -1 && action == m.Action.Substring(0, m.Action.IndexOf('/')));
            }
            object apiController = new GenericController(this.route.Domain, this.route.Aggregate);
            if (!(this.route.Controller is null)) {
                apiController = Activator.CreateInstance(this.route.Controller);
            }
            IHttpActionResult result = null;
            if (method.Argument is null) {
                
                result = await (apiController.ExecuteMethod(method.Name, Type.EmptyTypes)
                    as Task<IHttpActionResult>);
            } else {
                var routeData = request.GetRequestContext().RouteData;
                var arguments = method.Action.SlicePartials(begin: '{', end: new char[] { '}', ':' })
                    .Select(arg => Convert.ChangeType(routeData.Values[arg], method.Argument));
                result = await (apiController.ExecuteMethod(method.Name, Type.EmptyTypes, arguments.ToArray())
                    as Task<IHttpActionResult>);
            }
            var executed = await result.ExecuteAsync(new CancellationToken());

            return executed;
        }

    }

}
