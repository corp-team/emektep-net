﻿using HDS.App.Api.Common;
using HDS.App.Domain.Data;
using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace HDS.App.Api.Static {
    public static class SAPIValues {

        public static string CurrentRPIKey { get; set; } = "alem";
        public static string CurrentRPIVersion { get; set; } = "v1";

        private static string currentDebugHost;
        private static string currentProductionHost;

        public static string CurrentDebugHost {
            private get => String.IsNullOrEmpty(currentDebugHost) ? throw new Exception("Current Debug Host Not Set") : currentDebugHost;
            set => currentDebugHost = value;
        }
        public static string CurrentProductionHost {
            private get => String.IsNullOrEmpty(currentProductionHost) ? throw new Exception("Current Production Host Not Set") : currentProductionHost;
            set => currentProductionHost = value;
        }

        public static string CurrentRemoteHost {
            get {
                return $"{(SDbParams.LocalMachine ? CurrentDebugHost : CurrentProductionHost) }";
            }
        }

        private static string configurationXMLPath;
        public static string ConfigurationXMLPath {
            private get => String.IsNullOrEmpty(configurationXMLPath) ? throw new Exception("Configuration XML Path Not Set") : configurationXMLPath;
            set => configurationXMLPath = value;
        }

        public static APIConfig LoadConfiguration() {

            var xml = XDocument.Load(ConfigurationXMLPath);
            XDocument commonXml = null;
            using (var reader = new StringReader(XmlResources.common_api)) {
                commonXml = XDocument.Load(reader);
            }

            var routes = xml.Root.Descendants("routes");
            var commonRoutes = commonXml.Root.Descendants("routes");
            var query = (from r in routes.Descendants("route")
                         select new Route {
                             Key = r.Attributes("key").First().Value,
                             Controller = Type.GetType(r.Attributes("controller").FirstOrDefault()?.Value ?? ""),
                             Domain = Type.GetType(r.Attributes("domain").First().Value),
                             Aggregate = Type.GetType(r.Attributes("aggregate").First().Value),
                             Methods = (from m in r
                                        .Descendants("methods")
                                        .Descendants("method")
                                        select new Method {
                                            Name = m.Attributes("name").First().Value,
                                            Action = m.Attributes("action").First().Value,
                                            Argument = Type.GetType(m.Attributes("argument").FirstOrDefault()?.Value ?? "")
                                        }).Union(from cm in commonRoutes
                                                 .Descendants("common")
                                                 .Descendants("methods")
                                                 .Descendants("method")
                                                 select new Method {
                                                     Name = cm.Attributes("name").First().Value,
                                                     Action = cm.Attributes("action").First().Value,
                                                     Argument = Type.GetType(cm.Attributes("argument").FirstOrDefault()?.Value ?? "")
                                                 })
                         });
            return new APIConfig {
                ApiKey = xml.Root.Attributes().Single(a => a.Name == "key").Value,
                Routes = query.ToArray()
            };
        }

        internal static string CurrentRemoteAddress {
            get {
                return $"{CurrentRemoteHost}/{CurrentRPIKey}/{CurrentRPIVersion}";
            }
        }

    }
}