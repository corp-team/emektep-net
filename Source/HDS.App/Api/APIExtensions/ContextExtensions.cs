﻿using System.Web.Http.Controllers;

namespace HDS.App.Api.APIExtensions {

    public static class ContextExtensions {

        public static string GetRouteTemplate(this HttpActionContext actionContext) {

            return actionContext.ControllerContext.RouteData.Route.RouteTemplate;

        }

    }

}
