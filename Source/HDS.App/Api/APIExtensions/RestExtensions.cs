﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace HDS.App.Api.APIExtensions {
    public static class RestExtensions {

        private static HttpClient GenerateHttpclient(this string baseAddress, NameValueCollection headers = null) {

            var client = new HttpClient() {
                BaseAddress = new Uri(baseAddress)
            };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            headers = headers ?? new NameValueCollection();
            foreach (string header in headers) {
                client.DefaultRequestHeaders.Add(header, headers[header]);
            }
            return client;

        }

        public static async Task<RestResponse<TGETResponse>> RequestRoute<TGETResponse>(this string baseAddress, string route, NameValueCollection headers = null)
            where TGETResponse : class {

            var client = baseAddress.GenerateHttpclient(headers);
            var response = await client.GetAsync(route);

            if (response.IsSuccessStatusCode) {
                var obj = await response.Content.ReadAsAsync<TGETResponse>(new[] {
                    new JsonMediaTypeFormatter()
                });
                return new RestResponse<TGETResponse>() {
                    ResponseObject = obj,
                    ReasonPhrase = response.ReasonPhrase,
                    StatusCode = response.StatusCode,
                    Success = response.IsSuccessStatusCode,
                    Content = await response.Content.ReadAsStringAsync()
                };
            } else {
                return new RestResponse<TGETResponse>() {
                    ReasonPhrase = response.ReasonPhrase,
                    StatusCode = response.StatusCode,
                    Success = response.IsSuccessStatusCode,
                    Content = await response.Content.ReadAsStringAsync()
                };
            }

        }
        public static async Task<RestResponse<TGETResponse>> RequestRouteAsObject<TGETResponse>(this string baseAddress, string route, Func<JObject, TGETResponse> mapper, NameValueCollection headers = null)
            where TGETResponse : class {

            var client = baseAddress.GenerateHttpclient(headers);
            var response = await client.GetAsync(route);

            if (response.IsSuccessStatusCode) {
                var obj = mapper(JObject.Parse(await response.Content.ReadAsStringAsync()));
                return new RestResponse<TGETResponse>() {
                    ResponseObject = obj,
                    ReasonPhrase = response.ReasonPhrase,
                    StatusCode = response.StatusCode,
                    Success = response.IsSuccessStatusCode,
                    Content = await response.Content.ReadAsStringAsync()
                };
            } else {
                return new RestResponse<TGETResponse>() {
                    ReasonPhrase = response.ReasonPhrase,
                    StatusCode = response.StatusCode,
                    Success = response.IsSuccessStatusCode,
                    Content = await response.Content.ReadAsStringAsync()
                };
            }

        }
        public static async Task<RestResponse<IEnumerable<TGETResponse>>> RequestRouteAsArray<TGETResponse>(this string baseAddress, string route, Func<JArray, IEnumerable<TGETResponse>> mapper, NameValueCollection headers = null)
            where TGETResponse : class {

            var client = baseAddress.GenerateHttpclient(headers);
            var response = await client.GetAsync(route);

            if (response.IsSuccessStatusCode) {
                var arr = mapper(JArray.Parse(await response.Content.ReadAsStringAsync()));
                return new RestResponse<IEnumerable<TGETResponse>>() {
                    ResponseObject = arr,
                    ReasonPhrase = response.ReasonPhrase,
                    StatusCode = response.StatusCode,
                    Success = response.IsSuccessStatusCode,
                    Content = await response.Content.ReadAsStringAsync()
                };
            } else {
                return new RestResponse<IEnumerable<TGETResponse>>() {
                    ReasonPhrase = response.ReasonPhrase,
                    StatusCode = response.StatusCode,
                    Success = response.IsSuccessStatusCode,
                    Content = await response.Content.ReadAsStringAsync()
                };
            }

        }

        public static async Task<RestResponse<TResponse>> RequestPost<TResponse, TEntity>(this string baseAddress, TEntity entity, string route, NameValueCollection headers = null)
            where TEntity : class {

            var client = baseAddress.GenerateHttpclient(headers);
            var response = await client.PostAsync(route, entity, new JsonMediaTypeFormatter());

            if (response.IsSuccessStatusCode) {
                var obj = await response.Content.ReadAsAsync<TResponse>(new[] {
                    new JsonMediaTypeFormatter()
                });
                return new RestResponse<TResponse>() {
                    ResponseObject = obj,
                    ReasonPhrase = response.ReasonPhrase,
                    StatusCode = response.StatusCode,
                    Success = response.IsSuccessStatusCode,
                    Content = await response.Content.ReadAsStringAsync()
                };
            } else {
                return new RestResponse<TResponse>() {
                    ReasonPhrase = response.ReasonPhrase,
                    StatusCode = response.StatusCode,
                    Success = response.IsSuccessStatusCode,
                    Content = await response.Content.ReadAsStringAsync()
                };
            }

        }

        public static async Task<RestResponse> RequestDelete(this string baseAddress, string route, NameValueCollection headers = null) {

            var client = baseAddress.GenerateHttpclient(headers);
            var response = await client.DeleteAsync($"{baseAddress}/{route}");
            return new RestResponse() {
                ReasonPhrase = response.ReasonPhrase,
                StatusCode = response.StatusCode,
                Success = response.IsSuccessStatusCode,
                Content = await response.Content.ReadAsStringAsync()
            };

        }

    }
}
