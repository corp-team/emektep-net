﻿namespace HDS.App.Api.Common {
    public class Session {

        public ApiToken Token { get; set; } = new ApiToken();
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ClientMessage { get; set; }

    }
}
