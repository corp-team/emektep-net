﻿namespace HDS.App.Api.Common {

    public enum EApiRoutes {

        List = 1,
        All,
        Translations,
        Columns,
        Fields,
        One,
        Insert,
        Edit,
        Delete
    }

}
