﻿using System;
using System.Collections.Generic;

namespace HDS.App.Api.Common {
    public class APIConfig {

        public string ApiKey { get; internal set; }
        public IEnumerable<Route> Routes { get; internal set; }

    }

    public class Route {

        public string Key { get; set; }
        public Type Controller { get; set; }
        public Type Domain { get; set; }
        public Type Aggregate { get; set; }
        public IEnumerable<Method> Methods { get; internal set; }
    }

    public class Method {

        public string Name { get; internal set; }
        public string Action { get; internal set; }
        public Type Argument { get; internal set; }
    }

}
