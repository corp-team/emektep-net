﻿namespace HDS.App.Api.Common {
    public class ApiToken {

        public string AccessToken { get; set; }
        public bool Success { get; set; }

    }
}
