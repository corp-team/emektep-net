﻿using HDS.App.Api.Contracts;
using HDS.App.Api.Static;
using HDS.App.Domain.Aggregate;
using HDS.App.Domain.Contracts;
using HDS.App.Domain.DML;
using HDS.App.Domain.Objects;
using HDS.App.Engines.Core;
using HDS.App.Extensions.Decorators;
using HDS.App.Extensions.Static;
using HDS.App.Internalization.Strings;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Resources;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace HDS.App.Api.Common {

    public class GenericController : ApiController, IApiController {

        #region Ctor

        public GenericController(Type domainType, Type aggregateType) {
            ControllerContext.Configuration = new HttpConfiguration();
            Request = new HttpRequestMessage();
            DomainType = domainType;
            AggregateType = aggregateType;
            Config = new Lazy<APIConfig>(() => SAPIValues.LoadConfiguration());
        }

        #endregion

        #region Properties

        public Type DomainType { get; private set; }
        public Type AggregateType { get; private set; }
        public static Lazy<APIConfig> Config { get; private set; }

        public string ListRoute => "list";

        public static Dictionary<EApiRoutes, string> Routes => new Dictionary<EApiRoutes, string>() {
            { EApiRoutes.All, "all" },
            { EApiRoutes.Columns, "columns" },
            { EApiRoutes.Fields, "fields" },
            { EApiRoutes.List, "list" },
            { EApiRoutes.One, "one/{id:int}" },
            { EApiRoutes.Translations, "translations" },
            { EApiRoutes.Insert, "insert" },
            { EApiRoutes.Edit, "edit" },
            { EApiRoutes.Delete, "delete" },
        }.Select(pair => new KeyValuePair<EApiRoutes, string>(pair.Key,
            $"{SAPIValues.CurrentRPIKey}/{SAPIValues.CurrentRPIVersion}/{nameof(DomainType)}/{pair.Value}")).ToDictionary(p => p.Key, p => p.Value);

        #endregion

        #region Post Methods
        public async Task<IHttpActionResult> Insert([FromBody]IDOBase domain) {

            return await Task.FromResult(Json(domain.InsertSelf()));

        }

        public async Task<IHttpActionResult> Insert([FromBody]IAGBase aggregate, params Type[] referencedDomains) {
            var domain = aggregate.GetMappedDomainByType(DomainType);
            var allSucceeded = true;
            Exception fault = null;
            foreach (var rdType in referencedDomains) {
                if (!rdType.IsSubclassOf(typeof(DOBase<>).MakeGenericType(rdType))) {
                    throw new ArgumentException($"Type: {rdType.ToString()} Not Assignable From DOBase<{rdType.ToString()}>");
                }
                var rdomain = aggregate.GetMappedDomainByType(rdType);
                var single = typeof(CR).ExecuteStaticMethod("Single", new Type[] { rdType }, rdomain);
                IDMLResponse dmlResponse = single.ExecuteMethod("UpsertSync", new Type[] { }) as IDMLResponse;
                if (dmlResponse.Success) {
                    domain.GetReferencingKeyPairs(rdType).SingleOrDefault(
                        refk => refk.foreign.ReflectedType.IsAssignableFrom(typeof(DOBase<>).MakeGenericType(rdType)))
                        .referencing.SetValue(domain, dmlResponse.EntityID);
                } else {
                    allSucceeded = false;
                    fault = new Exception("Waterfall Exception Occurred. See Inner Exceptions", dmlResponse.Fault);
                    fault = fault.InnerException;
                }
            }

            if (allSucceeded) {
                return await Task.FromResult(Json(domain.InsertSelf()));
            } else {
                return await Task.FromResult(Json(new DMLResponse<object> { Success = false, Fault = fault }));
            }
        }

        public async virtual Task<IHttpActionResult> Edit([FromBody]IAGBase aggregate) {

            var eng = GenerateDOEngine();
            return Json(await eng.GenericUpdateAggregate(aggregate));

        }

        public async virtual Task<IHttpActionResult> Delete(long id) {
            var eng = GenerateDOEngine();
            return Json(await eng.GenericDelete(id));
        }

        #endregion

        #region Get Methods
        public async Task<IHttpActionResult> All() {
            var eng = GenerateAGEngine();
            return Json(await eng.GenericSelectAll());
        }

        public async Task<IHttpActionResult> One(long id) {
            var eng = GenerateAGEngine();
            return Json(await eng.GenericSelectByID(id));
        }

        public async Task<IHttpActionResult> List() {
            var eng = GenerateDOEngine();
            var response = await eng.GenericSelectAll();
            if (response.Success) {
                var list = response.DynamicCollectionResponse.Select(q =>
                    new { id = q.ID, text = q.TextValue }).ToList();
                list.Insert(0, new { id = (dynamic)0L, text = (dynamic)Preformats.FieldPlaceholder.Puts(AggregateType.GetStringResource("TitleEntity")) });
                return Json(list.AsEnumerable());
            } else {
                return Json(response);
            }
        }

        public async Task<IHttpActionResult> Translations() {
            var rm = new ResourceManager(AggregateType.GetCustomAttribute<ResourceLocatorAttribute>()?.ResourceType);
            var set = rm.GetResourceSet(Thread.CurrentThread.CurrentCulture, true, true);
            var dict = new Dictionary<string, string>();
            foreach (DictionaryEntry it in set) {
                string key = it.Key.ToString();
                string resource = it.Value.ToString();
                dict[key] = resource;
            }
            return await Task.FromResult(Json(dict));
        }

        public async Task<IHttpActionResult> Columns() {
            var list = PickColumns((pi) => pi.GetCustomAttribute<TableColumnAttribute>() != null).ToList();
            return await Task.FromResult(Json(list));
        }

        public async Task<IHttpActionResult> Fields(EFormType formType) {
            var list = PickColumns((pi) => (pi.GetCustomAttribute<FormFieldAttribute>()?.FormType & formType) == formType
                || pi.HasAttribute<HavingCollectionAttribute>()).ToList();
            return await Task.FromResult(Json(list));
        }

        #endregion

        #region Special Methods
        private IEnumerable<IDictionary<string, object>> PickColumns(Func<PropertyInfo, bool> filter = null) {

            var list = new List<IDictionary<string, object>>();
            var resourceType = AggregateType.GetCustomAttribute<ResourceLocatorAttribute>()?.ResourceType;
            var rm = resourceType != null ? new ResourceManager(resourceType) : null;
            foreach (var col in AggregateType.GetProperties()
                .Where(p => p.HasAttribute<TableColumnAttribute>())
                .Where(p => filter?.Invoke(p) ?? true)
                .OrderBy(prop => prop.GetCustomAttribute<TableColumnAttribute>().Order)) {
                var selectables = new List<object>();
                var dataType = GetDataTypeOfEditable(col, ref selectables);
                var dict = new Dictionary<string, object> {
                    { "name", col.Name },
                    { "defaultValue", null },
                    { "isRequired", (Activator.CreateInstance(AggregateType) as IAGBase).IsColumnRequired(col) },
                    {
                        "formdata",
                        new {
                            placeholder = Preformats.FieldPlaceholder.Puts(AggregateType.GetStringResource(col.Name)),
                            title = $"{AggregateType.GetStringResource(col.Name)}:",
                            isNavigationProperty = false,
                            dataType = dataType,
                            selectables = dataType == "selectable" ? selectables : null
                        }
                    }
                };
                if (filter == null) {
                    dict.Add("label", rm?.GetString(col.Name, Thread.CurrentThread.CurrentCulture) ?? col.Name);
                } else if (filter(col)) {
                    dict.Add("label", rm?.GetString(col.Name, Thread.CurrentThread.CurrentCulture) ?? col.Name);
                } else {
                    continue;
                }
                list.Add(dict);
            }
            return list.ToArray();
        }

        private static string GetDataTypeOfEditable(PropertyInfo col, ref List<object> selectables) {
            var dataType = "text";
            if (col.PropertyType.OfType<DateTime>()) {
                dataType = "datetime";
            } else if (col.PropertyType.OfType<bool>()) {
                dataType = "boolean";
            } else if (col.PropertyType.IsNumericType()) {
                dataType = "numeric";
            } else if (col.PropertyType.IsCollection() || col.HasAttribute<HavingCollectionAttribute>()) {
                dataType = "collection";
            } else if (col.HasAttribute<ComputedFieldAttribute>()) {
                dataType = "computed";
                var attr = col.GetCustomAttribute<ComputedFieldAttribute>();
                if (attr.ComputedType == EComputedType.Enum) {
                    dataType = "selectable";
                    foreach (var val in Enum.GetValues(attr.ReferencedType)) {
                        selectables.Add(new { value = Enum.GetName(attr.ReferencedType, val), text = val.GetEnumResource(attr.ReferencedType) });
                    }
                } else if (attr.ComputedType == EComputedType.FormattedString) {
                    if (attr.ReferencedType.Equals(typeof(DateTime))) {
                        dataType = "datetime";
                    }
                }
            }

            return dataType;
        }

        private IGenericEngine GenerateDOEngine() {
            return Activator.CreateInstance(typeof(DOEngine<>).MakeGenericType(DomainType)) as IGenericEngine;
        }

        private IGenericEngine GenerateAGEngine() {
            return Activator.CreateInstance(typeof(AGEngine<>).MakeGenericType(AggregateType)) as IGenericEngine;
        }

        #endregion


    }

}
