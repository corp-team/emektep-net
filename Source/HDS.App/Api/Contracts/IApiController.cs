﻿using HDS.App.Api.Common;
using HDS.App.Domain.Aggregate;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Decorators;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace HDS.App.Api.Contracts {

    public interface IApiController {

        string ListRoute { get; }
        Task<IHttpActionResult> Translations();
        Task<IHttpActionResult> Columns();
        Task<IHttpActionResult> Fields(EFormType type);

        Task<IHttpActionResult> List();
        Task<IHttpActionResult> All();
        Task<IHttpActionResult> One(long id);

        Task<IHttpActionResult> Insert([FromBody]IDOBase domain);
        Task<IHttpActionResult> Insert([FromBody]IAGBase aggregate, params Type[] referencedDomains);
        Task<IHttpActionResult> Edit([FromBody]IAGBase aggregate);
        Task<IHttpActionResult> Delete(long id);

    }

}
