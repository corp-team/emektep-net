﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration.Configuration;

namespace HDS.App.Extensions.EFExtensions {
    public static class EntityFrameworkExtensions {

        public static PrimitivePropertyConfiguration HasUniqueIndex(this PrimitivePropertyConfiguration cfg) {
            return cfg.HasColumnAnnotation(IndexAnnotation.AnnotationName, 
                new IndexAnnotation(new IndexAttribute() { IsUnique = true }));
        }

    }
}
