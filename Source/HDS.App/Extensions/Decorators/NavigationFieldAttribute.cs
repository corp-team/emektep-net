﻿using System;

namespace HDS.App.Extensions.Decorators {
    [System.AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class NavigationFieldAttribute : Attribute {
        
        public NavigationFieldAttribute(Type navigationType) {

            this.NavigationType = navigationType;

        }

        public Type NavigationType { get; private set; }
    }

}
