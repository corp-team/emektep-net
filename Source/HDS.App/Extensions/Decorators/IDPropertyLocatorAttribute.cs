﻿using System;

namespace HDS.App.Extensions.Decorators {

    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class IDPropertyLocatorAttribute : Attribute {

        public IDPropertyLocatorAttribute() {
        }

    }

}
