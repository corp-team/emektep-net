﻿using System;

namespace HDS.App.Extensions.Decorators {
    [System.AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class HavingCollectionAttribute : Attribute {
        // This is a positional argument
        public HavingCollectionAttribute() {
        }
    }
}