﻿using System;

namespace HDS.App.Extensions.Decorators {
    [System.AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class TableColumnAttribute : Attribute {
        public TableColumnAttribute() {
        }
        
        public int Order { get; set; }
    }
}
