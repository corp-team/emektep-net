﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;

namespace HDS.App.Extensions.Static {
    public static class ParseExtensions {

        public static TObject Set<TObject>(this TObject obj, params Action<TObject>[] commits) {

            if (obj != null) {
                foreach (var commit in commits) {
                    commit(obj);
                }
            }
            return obj;
        }

        public static int ToInt<TItem>(this TItem self) {

            var outInt = -1;
            int.TryParse(self.ToString(), out outInt);
            return outInt;

        }

        public static Guid ToGuid<TItem>(this TItem self) {

            var outGuid = Guid.Empty;
            Guid.TryParse(self.ToString(), out outGuid);
            return outGuid;

        }

        public static DateTime ToDateTime<TItem>(this TItem self) {

            var outdt = DateTime.MinValue;
            DateTime.TryParse(self.ToString(), out outdt);
            return outdt;

        }

        public static TOut ParseTo<TOut>(this object self) {

            return (TOut)self.ParseTo(typeof(TOut));

        }

        public static object ParseTo(this object self, Type type) {
            if (type.Equals(typeof(DBNull))) {
                return default(DBNull);
            } else if (self.GetType().isConvertible()) {
                return Convert.ChangeType(self, type);
            } else if (self.ToInt() != -1) {
                return (object)self.ToInt();
            } else if (self.ToGuid() != Guid.Empty) {
                return (object)self.ToGuid();
            } else if (self.ToDateTime() != DateTime.MinValue) {
                return (object)self.ToDateTime();
            } else {
                return self;
            }
        }

        public static bool isConvertible(this Type type) {
            return type.GetInterfaces().Any(intf => intf.Equals(typeof(IConvertible)));
        }

        public static string ToJson<TItem>(this TItem item) {

            return JsonConvert.SerializeObject(item);

        }

        public static TItem FromJson<TItem>(this string self) {

            return JsonConvert.DeserializeObject<TItem>(self);

        }

        public static T FromJToken<T>(this JToken jToken, string key) {

            dynamic ret = jToken[key];
            if (ret == null) return default(T);
            if (ret is JObject) return JsonConvert.DeserializeObject<T>(ret.ToString());
            return (T)ret;

        }

    }
}
