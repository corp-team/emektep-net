﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Routing;
using System.Web.Routing;

namespace HDS.App.Extensions.Static {

    public static class HttpExtensions {

        public static IDictionary<string, object> SelectRouteValues(this RouteData routeData) {
            if (!routeData.Values.ContainsKey("MS_SubRoutes"))
                return new Dictionary<string, object>();

            return ((IHttpRouteData[])routeData.Values["MS_SubRoutes"]).SelectMany(x => x.Values).ToDictionary(x => x.Key, y => y.Value);
        }

    }

}
