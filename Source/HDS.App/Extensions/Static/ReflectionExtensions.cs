﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
namespace HDS.App.Extensions.Static {

    public static class ReflectionExtensions {

        public static IEnumerable<PropertyInfo> ResolveFields<TEntity>(this TEntity self) {

            return typeof(TEntity).GetProperties();

        }

        public static IEnumerable<PropertyInfo> ResolveProperties<TEntity>(this TEntity self) {

            return typeof(TEntity).GetProperties(BindingFlags.Instance | BindingFlags.Public);

        }

        public static void SetValue<TEntity>(this MemberInfo self, TEntity entity, object value) {
            if (self is PropertyInfo prop) {
                prop.SetValue(entity, value);
            } else if (self is FieldInfo field) {
                field.SetValue(entity, value);
            }
        }

        public static object GetValue<TEntity>(this MemberInfo self, TEntity entity) {
            if (self is PropertyInfo prop) {
                return prop.GetValue(entity);
            } else if (self is FieldInfo field) {
                return field.GetValue(entity);
            } else {
                return null;
            }
        }

        public static Type GetMemberType(this MemberInfo self) {
            if (self is PropertyInfo prop) {
                return prop.PropertyType;
            } else if (self is FieldInfo field) {
                return field.FieldType;
            } else {
                return null;
            }
        }

        public static bool IsAssigned<TItem>(this MemberInfo self, TItem item) {

            var value = self.GetValue(item);
            if (value != null) {
                if (self.GetMemberType().IsEnum) {
                    return true;
                }/* else if (self.PropertyType.IsNumericType()) {
                    return value.ToInt() != 0;
                } */else if (self.GetMemberType().Equals(typeof(DateTime))) {
                    DateTime dt = DateTime.MinValue;
                    DateTime.TryParse(value.ToString(), out dt);
                    return dt != DateTime.MinValue && dt != DateTime.MaxValue;
                } else {
                    return value.ToString() != "";
                }
            } else {
                return false;
            }

        }

        public static bool IsNumericType(this Type t) {
            switch (Type.GetTypeCode(t)) {
                case TypeCode.Byte:
                case TypeCode.SByte:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Single:
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsConvertible(this Type self) {

            return self.GetInterfaces().Any(inf => inf.Equals(typeof(IConvertible)));

        }

        public static bool IsCollection(this Type self) {

            return self.GetInterfaces().Any(inf => new[] { typeof(ICollection<>), typeof(IList<>) }.ToList()
                .Any(type => inf.IsGenericType && inf.Equals(type.MakeGenericType(inf.GetGenericArguments()))));

        }

        public static bool IsEnumerable(this Type self) {

            return self.GetInterfaces().Any(inf => new[] { typeof(ICollection<>), typeof(IList<>), typeof(IEnumerable<>) }.ToList()
                .Any(type => inf.IsGenericTypeDefinition && inf.Equals(type.MakeGenericType(inf.GetGenericArguments()))));

        }
        public static bool Implements(this Type self, Type type) {

            return self.GetInterfaces().Any(inf => inf.IsGenericTypeDefinition ?
                inf.Equals(type.MakeGenericType(inf.GetGenericArguments())) : inf.Equals(type));

        }
        public static bool HasGenericArgument(this Type self, Type type) {

            return self.GetInterfaces().Any(inf => inf.IsGenericTypeDefinition &&
                inf.GetGenericArguments().Any(ga => ga.Equals(type)));

        }
        public static TInterface Breath<TInterface>(this Type self)
            where TInterface : class {

            return Activator.CreateInstance(self) as TInterface;

        }
        public static bool OfType<T>(this Type self) {

            return self.Equals(typeof(T));

        }
        public static bool IsTypeOf<T>(this object self) {

            return self.GetType().OfType<T>();

        }

        public static bool HasAttribute<TAttribute>(this PropertyInfo self)
            where TAttribute : Attribute {
            return self.GetCustomAttribute<TAttribute>() != null;
        }

        public static Type GetTypeByName(this Assembly assembly, string name) {
            return assembly.GetTypes().SingleOrDefault(t => t.Name == name);
        }

        public static T GetInstanceByName<T>(this Assembly assembly, string name)
            where T : class {
            var t = assembly.GetTypeByName(name);
            return Activator.CreateInstance(t) as T;
        }

        public static object ExecuteMethod(this object self, string methodName, Type[] genericTypes,
            params object[] arguments) {

            var type = self.GetType();
            var mi = type.GetMethod(methodName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.FlattenHierarchy);
            genericTypes.ToList().ForEach(gt => mi = mi.MakeGenericMethod(gt));
            if (mi == null) {
                throw new NullReferenceException($"{methodName} cannot be found on type: {type.ToString()}");
            }
            try {
                return mi.Invoke(self, arguments.ToArray());
            } catch (Exception ex) {
                var willThrown = new Exception($"{methodName} threw inner exception on type: {type.ToString()}", ex);
                throw willThrown;
            }
        }

        public static object ExecuteStaticMethod(this Type staticType, string methodName, Type[] genericTypes,
            params object[] arguments) {

            var mi = staticType.GetMethod(methodName, BindingFlags.Public | BindingFlags.Static);
            genericTypes.ToList().ForEach(gt => mi = mi.MakeGenericMethod(gt));
            return mi.Invoke(null, arguments.ToArray());

        }

        public static bool IsDebugBuild(this Assembly assembly) {
            var attribute = assembly.GetCustomAttribute<DebuggableAttribute>();
            return attribute == null ? false : attribute.IsJITTrackingEnabled;
        }

        public static Assembly GetReferencingAssembly(this StackTrace trace, int overflow = 100) {
            var a = Assembly.GetExecutingAssembly();
            var k = 1;
            while (a == Assembly.GetExecutingAssembly() && k <= overflow) {
                a = new StackTrace().GetFrame(k++).GetMethod().ReflectedType.Assembly;
            }
            return a;
        }

    }

}
