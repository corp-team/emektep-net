﻿using HDS.App.Domain.Objects;
using System;

namespace HDS.App.Domain.DML {

    public interface ISibling<TEntity>
        where TEntity : DOBase<TEntity> {

        ISibling<TEntity> Next<TOther>(Func<IExecutedResult<TEntity>, TOther> cursor) where TOther : DOBase<TOther>;
        TEntity Execute(Action<IExecutedResultList> cursor);

    }

}