﻿using HDS.App.Domain.Objects;
using System.Threading.Tasks;

namespace HDS.App.Domain.DML {

    public interface ISinglePersister<TEntity>
        where TEntity : class {

        Task<DMLResponse<TEntity>> Persist();

    }

}