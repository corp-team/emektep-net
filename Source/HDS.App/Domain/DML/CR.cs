﻿using HDS.App.Domain.Objects;

namespace HDS.App.Domain.DML {

    public static class CR {

		public static ISingle<TEntity> Single<TEntity>(TEntity entity)
			where TEntity : DOBase<TEntity> {
			return new Single<TEntity>(entity);
		}

	}

}
