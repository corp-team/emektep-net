﻿using System;
using System.Collections.Generic;

namespace HDS.App.Domain.Contracts {

    public interface IDMLResponse {

        Exception Fault { get; set; }
        bool Success { get; set; }
        string Message { get; set; }
        long EntityID { get; }

        IEnumerable<dynamic> DynamicCollectionResponse { get; set; }
        dynamic DynamicSingleResponse { get; set; }
    }

}
