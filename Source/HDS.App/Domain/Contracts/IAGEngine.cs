﻿using HDS.App.Domain.Aggregate;
using HDS.App.Domain.Objects;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace HDS.App.Domain.Contracts {

    public interface IAGEngine<TAggregate>
            where TAggregate : AGBase<TAggregate> {
        Task<DMLResponse<TAggregate>> SelectAll();
        Task<DMLResponse<TAggregate>> SelectBy(params Expression<Func<TAggregate, bool>>[] selectors);
        Task<DMLResponse<TAggregate>> SelectSingle(Expression<Func<TAggregate, bool>> selector);
        Task<DMLResponse<TAggregate>> SelectByID(long id);

        DMLResponse<TAggregate> SelectAllSync();
        DMLResponse<TAggregate> SelectBySync(params Expression<Func<TAggregate, bool>>[] selectors);
        DMLResponse<TAggregate> SelectSingleSync(Expression<Func<TAggregate, bool>> selector);
        DMLResponse<TAggregate> SelectByIDSync(long id);
    }

}
