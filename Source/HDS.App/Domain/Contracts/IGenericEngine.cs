﻿using HDS.App.Domain.Aggregate;
using System.Threading.Tasks;

namespace HDS.App.Domain.Contracts {
    public interface IGenericEngine {
        Task<IDMLResponse> GenericSelectAll();
        Task<IDMLResponse> GenericSelectByID(long id);

        IDMLResponse GenericSelectAllSync();
        IDMLResponse GenericSelectByIDSync(long id);
        Task<IDMLResponse> GenericDelete(long id);
        Task<IDMLResponse> GenericUpdateAggregate(IAGBase aggregate);
    }

}
