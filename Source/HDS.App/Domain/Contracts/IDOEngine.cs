﻿using HDS.App.Domain.Aggregate;
using HDS.App.Domain.Objects;
using HDS.App.Domain.Query;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace HDS.App.Domain.Contracts {
    public interface IDOEngine<TEntity>
        where TEntity : DOBase<TEntity> {

        Task<DMLResponse<TEntity>> Insert(TEntity entity);
        Task<DMLResponse<TEntity>> InsertIfAbsent<TProp>(TEntity entity, Func<TEntity, TProp> discriminator);
        Task<DMLResponse<TEntity>> Update(long id, Func<IUpdateClause<TEntity>, IUpdateClause<TEntity>> updater);
        Task<IDMLResponse> UpdateAggregate(IAGBase aggregate);
        Task<DMLResponse<TEntity>> Delete<TKey>(TKey key);

        Task<DMLResponse<TEntity>> SelectAll();
        Task<DMLResponse<TEntity>> SelectSingleByID<TKey>(TKey key);
        Task<DMLResponse<TEntity>> SelectLastRecord<TKey>(Expression<Func<TEntity, TKey>> keySelector);
        Task<DMLResponse<TEntity>> SelectBy(params Expression<Func<TEntity, bool>>[] selectors);
        Task<DMLResponse<TEntity>> SelectSingle(Expression<Func<TEntity, bool>> selector);

        DMLResponse<TEntity> SelectAllSync();
        DMLResponse<TEntity> SelectBySync(params Expression<Func<TEntity, bool>>[] selectors);
        DMLResponse<TEntity> SelectSingleSync(Expression<Func<TEntity, bool>> selector);
        DMLResponse<TEntity> SelectLastRecordSync<TKey>(Expression<Func<TEntity, TKey>> keySelector);
        DMLResponse<TEntity> SelectSingleByIDSync<TKey>(TKey key);

        DMLResponse<TEntity> InsertSync(TEntity entity);
        DMLResponse<TEntity> InsertIfAbsentSync<TProp>(TEntity entity, Func<TEntity, TProp> discriminator);
        DMLResponse<TEntity> UpdateSync(long id, Func<IUpdateClause<TEntity>, IUpdateClause<TEntity>> updater);
        IDMLResponse UpdateAggregateSync(IAGBase aggregate);
        DMLResponse<TEntity> DeleteSync<TKey>(TKey key);

    }

}
