﻿using HDS.App.Domain.Objects;
using System;

namespace HDS.App.Domain.Extensions {

    internal static class EnumExtensions {

        internal static EDataType GetDataType(this Type self) {

            if (self.Equals(typeof(String))) {
                return EDataType.String;
            } else if (self.Equals(typeof(bool))) {
                return EDataType.Bool;
            } else if (self.Equals(typeof(DateTime))) {
                return EDataType.DateTime;
            } else if (self.IsEnum) {
                return EDataType.Enum;
            } else if (self.Equals(typeof(int?)) || self.Equals(typeof(int))) {
                return EDataType.Int;
            } else if (self.Equals(typeof(long?)) || self.Equals(typeof(long))) {
                return EDataType.BigInt;
            } else if (self.Equals(typeof(decimal))) {
                return EDataType.Spatial;
            } else {
                return EDataType.String;
            }

        }

    }

}
