﻿using System.ComponentModel;

namespace HDS.App.Domain.Enums {
    public enum EOrderBy {
        [Description("DESC")]
        DESC,
        [Description("ASC")]
        ASC
    }
}
