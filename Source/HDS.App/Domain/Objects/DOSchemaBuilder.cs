﻿

using HDS.App.Domain.Data;
using HDS.App.Extensions.Static;

using System;

namespace HDS.App.Domain.Objects {

    public class DOSchemaBuilder : IDOSchemaBuilder {

        private string schemaName;
        private string tableName;

        public string FormatsBy(string format) {
            return format.Puts(this.schemaName, this.tableName);
        }

        public IDOSchemaBuilder SchemaName(string schemaName) {

            this.schemaName = SDbParams.CurrentServerType == EServerType.MySql ? schemaName.ToLower() : schemaName;
            return this;

        }

        public IDOSchemaBuilder TableName(string tableName) {

            this.tableName = SDbParams.CurrentServerType == EServerType.MySql ? tableName.ToLower() : tableName;
            return this;

        }
        public string GetTableName() {
            return this.tableName;
        }

        public string GetFormatted() {
            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    return "`{0}_{1}`".Puts(this.schemaName, this.tableName);
                case EServerType.MsSql:
                    return "[{0}].[{1}]".Puts(this.schemaName, this.tableName);
                default:
                    throw new NotImplementedException("Unsupported keyType");
            }
        }

        public string Build() {

            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    return GetFormatted();
                case EServerType.MsSql:
                    var script = @"
IF NOT EXISTS (SELECT schema_name 
    FROM INFORMATION_SCHEMA.SCHEMATA 
    WHERE schema_name = '{0}' )
BEGIN
    EXEC sp_executesql N'CREATE SCHEMA {0};';
END";
                    using (var engine = SDbParams.DataTools.GenerateEngine()) {
                        using (var command = engine.ConnectifiedCommand(script.Puts(schemaName))) {
                            command.ExecuteNonQuery();
                            command.Connection.Close();
                        }
                    }
                    return GetFormatted();
                default:
                    throw new NotImplementedException("Unsupported keyType");
            }

        }

    }

}