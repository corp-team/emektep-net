﻿
using HDS.App.Domain.Data;
using HDS.App.Extensions.Static;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace HDS.App.Domain.Objects {
    internal class DOPrimaryKeyBuilder : ILineBuilder {

        private IEnumerable<MemberInfo> primaryKeyProps;
        private IDOSchemaBuilder hostSchemaBuilder;

        public IEnumerable<MemberInfo> PrimaryKeyProps {
            get {
                return primaryKeyProps;
            }
        }

        public DOPrimaryKeyBuilder(IDOSchemaBuilder hostSchemaBuilder, IEnumerable<MemberInfo> primaryKeyProps) {

            this.hostSchemaBuilder = hostSchemaBuilder;
            this.primaryKeyProps = primaryKeyProps;

        }

        public string Build() {

            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                case EServerType.MsSql:
                    return "CONSTRAINT PK_{0} PRIMARY KEY ({1})".Puts(
                        this.hostSchemaBuilder.GetTableName(),
                        this.PrimaryKeyProps.Select(p => p.Name).Aggregate((prev, next) => "{0}, {1}".Puts(prev, next))
                    );
                default:
                    throw new NotImplementedException("Unsupported option");
            }

        }
    }
}