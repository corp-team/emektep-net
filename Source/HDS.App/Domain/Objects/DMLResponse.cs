﻿using HDS.App.Domain.Aggregate;
using HDS.App.Domain.Contracts;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HDS.App.Domain.Objects {

    public class DMLResponse<TEntity> : IDMLResponse
        where TEntity : class {

        private string _Message;

        [JsonIgnore]
        public long EntityID {
            get {
                if (SingleResponse.GetType().IsSubclassOf(typeof(DOBase<>).MakeGenericType(typeof(TEntity)))) {
                    return (SingleResponse as IDOBase).ID;
                } else if (SingleResponse.GetType().IsSubclassOf(typeof(AGBase<>).MakeGenericType(typeof(TEntity)))) {
                    return (SingleResponse as IAGBase).IDProperty;
                } else {
                    throw new ArgumentException($"EntityID.get called on {SingleResponse.GetType().ToString()} which should have been of DOBase<> or AGBase<>");
                }
            }
        }
        public Exception Fault { get; set; }
        public bool Success { get; set; }
        public string Message {
            get {
                return Success ? _Message : Fault?.Message ?? "";
            }
            set {
                _Message = value;
            }
        }

        public DMLResponse() {
            Success = true;
        }

        private IEnumerable<TEntity> collectionResponse;
        public IEnumerable<TEntity> CollectionResponse {
            get => dynamicCollectionResponse as ICollection<TEntity> ?? collectionResponse;
            set => dynamicCollectionResponse = collectionResponse = value;
        }

        private IEnumerable<dynamic> dynamicCollectionResponse;
        [JsonIgnore]
        public IEnumerable<dynamic> DynamicCollectionResponse {
            get => collectionResponse.Select(cr => (dynamic)cr) ?? dynamicCollectionResponse;
            set => dynamicCollectionResponse = collectionResponse = value.Select(cr => cr as TEntity);
        }

        private TEntity singleResponse;
        public TEntity SingleResponse {
            get => dynamicSingleResponse as TEntity ?? singleResponse;
            set => dynamicSingleResponse = singleResponse = value;
        }

        private dynamic dynamicSingleResponse;
        [JsonIgnore]
        public dynamic DynamicSingleResponse {
            get => (dynamic)singleResponse ?? dynamicSingleResponse;
            set => dynamicSingleResponse = singleResponse = value as TEntity;
        }

    }
}
