﻿
using HDS.App.Domain.Data;
using HDS.App.Extensions.Static;

using System;
using System.Collections.Generic;
using System.Linq;

namespace HDS.App.Domain.Objects {
    internal class DOUniqueKeyBuilder : ILineBuilder {

        private IEnumerable<DOPropBuilder> uniqueKeyProps;
        private IDOSchemaBuilder hostSchemaBuilder;

        public DOUniqueKeyBuilder(IDOSchemaBuilder hostSchemaBuilder, IEnumerable<DOPropBuilder> uniqueKeyProps) {
            this.hostSchemaBuilder = hostSchemaBuilder;
            this.uniqueKeyProps = uniqueKeyProps;
        }

        public string Build() {
            if (this.uniqueKeyProps.Count() == 0) {
                return String.Empty;
            }
            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    var uniqueListString = "";
                    foreach (var uqProp in this.uniqueKeyProps) {
                        uniqueListString += $", {uqProp.Prop.Name}";
                        if (uqProp.Prop.GetMemberType().Equals(typeof(String))) {
                            uniqueListString += $"({uqProp.MaxLength})";
                        }
                    }
                    return "CONSTRAINT UQ_{0}_{1} UNIQUE ({2})".Puts(
                        this.hostSchemaBuilder.GetTableName(),
                        this.uniqueKeyProps.Select(p => p.Prop.Name).Aggregate((prev, next) =>
                            "{0}_{1}".Puts(prev, next)), uniqueListString.TrimStart(',').TrimStart(' '));
                case EServerType.MsSql:
                    return "CONSTRAINT UQ_{0}_{1} UNIQUE ({2})".Puts(
                        this.hostSchemaBuilder.GetTableName(),
                        this.uniqueKeyProps.Select(p => p.Prop.Name).Aggregate((prev, next) =>
                            "{0}_{1}".Puts(prev, next)),
                        this.uniqueKeyProps.Select(p => p.Prop.Name).Aggregate((prev, next) => "{0}, {1}".Puts(prev, next))
                    );
                default:
                    throw new NotImplementedException("Unsupported option");
            }

        }
    }
}