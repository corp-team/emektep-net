﻿namespace HDS.App.Domain.Objects {
    public interface IDomainBaseModel {
        string Slug { get; }
    }
}
