﻿
using HDS.App.Domain.Data;
using HDS.App.Extensions.Static;
using System;
using System.Collections.Generic;

namespace HDS.App.Domain.Objects {

    public enum EDataType {
        Text,
        String,
        Int,
        Bool,
        Enum,
        DateTime,
        Date,
        UniqueIdentifier,
        BigInt,
        Spatial
    }

    public static class EDataTypeExtensions {

        public static string Parse(this EDataType self, EServerType sType, int maxLength) {
            var responseString = "";
            switch (sType) {
                case EServerType.MySql:
                    switch (self) {
                        case EDataType.String:
                            responseString = maxLength == 0 ? "VARCHAR(64)" : "VARCHAR({0})".Puts(maxLength);
                            break;
                        case EDataType.Int:
                        case EDataType.Enum:
                            responseString = "INT" + (maxLength == 0 ? "(11)" : "({0})".Puts(maxLength));
                            break;
                        case EDataType.BigInt:
                            responseString = "BIGINT" + (maxLength == 0 ? "" : "({0})".Puts(maxLength));
                            break;
                        case EDataType.Spatial:
                            responseString = "DECIMAL(9,6)";
                            break;
                        case EDataType.Bool:
                            responseString = "BOOLEAN";
                            break;
                        case EDataType.DateTime:
                            responseString = "DATETIME";
                            break;
                        case EDataType.Date:
                            responseString = "DATE";
                            break;
                        case EDataType.UniqueIdentifier:
                            responseString = "CHAR(36)";
                            break;
                        case EDataType.Text:
                            responseString = "TEXT";
                            break;
                        default:
                            throw new KeyNotFoundException("invalid EDataType");
                    }
                    break;
                case EServerType.MsSql:
                    switch (self) {
                        case EDataType.String:
                            responseString = "VARCHAR" + (maxLength == 0 ? "(MAX)" : "({0})".Puts(maxLength));
                            break;
                        case EDataType.Int:
                        case EDataType.Enum:
                            responseString = "INT";
                            break;
                        case EDataType.BigInt:
                            responseString = "BIGINT";
                            break;
                        case EDataType.Spatial:
                            responseString = "DECIMAL(9,6)";
                            break;
                        case EDataType.Bool:
                            responseString = "Bit";
                            break;
                        case EDataType.DateTime:
                            responseString = "DATETIME2(7)";
                            break;
                        case EDataType.Date:
                            responseString = "DATE";
                            break;
                        case EDataType.UniqueIdentifier:
                            responseString = "UNIQUEIDENTIFIER";
                            break;
                        case EDataType.Text:
                            responseString = "VARCHAR(MAX)";
                            break;
                        default:
                            throw new KeyNotFoundException("invalid EDataType");
                    }
                    break;
                default:
                    throw new NotImplementedException("Unsupported option");
            }
            return responseString;

        }

    }
}