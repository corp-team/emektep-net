﻿namespace HDS.App.Domain.Objects {
    public interface IFormatter : ILineBuilder {

        string GetFormatted();

    }
}
