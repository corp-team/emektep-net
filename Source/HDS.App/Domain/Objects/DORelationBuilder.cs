﻿
using HDS.App.Domain.Data;
using HDS.App.Extensions.Static;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace HDS.App.Domain.Objects {

    public class DORelationBuilder : ILineBuilder {

        internal IEnumerable<MemberInfo> ReferencedKeys { get; private set; }
        internal IEnumerable<MemberInfo> ForeignKeys { get; private set; }

        private IDOSchemaBuilder hostSchemaBuilder;
        private IDOSchemaBuilder referencedSchemaBuilder;

        public DORelationBuilder(IDOSchemaBuilder hostSchemaBuilder, IEnumerable<MemberInfo> piForeignKeys) {

            this.hostSchemaBuilder = hostSchemaBuilder;
            this.ForeignKeys = piForeignKeys;

        }

        public void References<TDomain>(params Expression<Func<TDomain, long>>[] selectors)
            where TDomain : DOBase<TDomain> {

            var domain = Activator.CreateInstance<TDomain>();
            this.referencedSchemaBuilder = domain.SchemaBuilder;
            this.ReferencedKeys = selectors.Select(s => s.ResolveMember());

        }

        public string Build() {
            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                case EServerType.MsSql:
                    return "CONSTRAINT FK_{0}_{1} FOREIGN KEY({2}) REFERENCES {3}({4}) ON DELETE NO ACTION ON UPDATE NO ACTION".Puts(
                        this.hostSchemaBuilder.GetTableName(),
                        this.referencedSchemaBuilder.GetTableName(),
                        this.ForeignKeys.Select(p => p.Name).Aggregate((prev, next) => "{0},{1}".Puts(prev, next)),
                        this.referencedSchemaBuilder.GetFormatted(),
                        this.ReferencedKeys.Select(p => p.Name).Aggregate((prev, next) => "{0},{1}".Puts(prev, next))
                        );
                default:
                    throw new NotImplementedException("Unsupported option");
            }
        }

    }

}