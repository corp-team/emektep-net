﻿
using HDS.App.Domain.Objects;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HDS.App.Domain.Query {

    public interface IQueryExecuter<TEntity>
         where TEntity : DOBase<TEntity> {

        Task GenerateSingleExecutedQuery(Action<ExecutedQuery<TEntity>> doneback,
            Action noneback = null);
        Task<IEnumerable<IExecutedQuery<TEntity>>> GenerateExecutedQueryList(Action noneback = null);

    }

}