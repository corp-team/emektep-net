﻿using HDS.App.Domain.Objects;
using HDS.App.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace HDS.App.Domain.Query {

    public class ConfinedList<TEntity> : IConfinedList<TEntity>
        where TEntity : DOBase<TEntity> {

        private SelectList<TEntity> _SelectList;

        public ConfinedList(SelectList<TEntity> selectList, Expression<Func<TEntity, bool>> confineClause = null) {
            this._SelectList = selectList;
            if (confineClause != null) {
                this.And(confineClause);
            }
        }

        public IConfinedList<TEntity> And(Expression<Func<TEntity, bool>> confineClause) {

            GenerateConstraintParameters(confineClause.Body, out var operands, out var props, out var values);
            props.Zip(values, (p, v) => Tuple.Create(p, v)).Each((i, pv) => {
                if (i == 0) {
                    _SelectList.QueryBuilder.AndConstraint(pv.Item1, pv.Item2, ExpressionType.Equal);
                } else {
                    _SelectList.QueryBuilder.AndConstraint(pv.Item1, pv.Item2, operands.ElementAt(i));
                }
            });
            return this;
        }

        public IConfinedList<TEntity> Or(Expression<Func<TEntity, bool>> confineClause) {

            GenerateConstraintParameters(confineClause.Body, out var operands, out var props, out var values);
            props.Zip(values, (p, v) => Tuple.Create(p, v)).Each((i, pv) => {
                if (i == 0) {
                    _SelectList.QueryBuilder.OrConstraint(pv.Item1, pv.Item2, ExpressionType.Equal);
                } else {
                    _SelectList.QueryBuilder.OrConstraint(pv.Item1, pv.Item2, operands.ElementAt(i));
                }
            });
            return this;

        }

        public IOrderedList<TEntity> Order() {

            return new OrderedList<TEntity>(this._SelectList);

        }

        private static void GenerateConstraintParameters(Expression clause, out List<ExpressionType> operands, out List<MemberInfo> props, out List<object> values) {
            operands = new List<ExpressionType>();
            props = new List<MemberInfo>();
            values = new List<object>();

            if (clause is BinaryExpression binary) {
                operands.Add(binary.NodeType);
                GenerateConstraintParameters(binary.Right, out var ioperands, out var iprops, out var ivalues);
                GenerateConstraintParameters(binary.Left, out var iioperands, out var iiprops, out var iivalues);
                values.AddRange(ivalues);
                values.AddRange(iivalues);
                if ((binary.Right is BinaryExpression rbinary && rbinary.IsParameterTypeOf<TEntity>()) || ivalues.Count == 0) {
                    props.AddRange(iprops);
                    operands.AddRange(ioperands);
                }
                if ((binary.Left is BinaryExpression lbinary && lbinary.IsParameterTypeOf<TEntity>()) || iivalues.Count == 0) {
                    props.AddRange(iiprops);
                    operands.AddRange(iioperands);
                }
            } else if (clause is MemberExpression member) {
                var value = member.Resultify();
                if (value != null) {
                    values.Add(value);
                }
                props.Add(member.ExposeMember());
            } else if (clause is UnaryExpression unary) {
                var value = unary.Resultify();
                if (value != null) {
                    values.Add(value);
                }
                var prop = unary.ExposeMember();
                if (prop != null) {
                    props.Add(prop);
                }
            }


        }

        public async Task ExecuteOne(Action<IExecutedQuery<TEntity>> cursor, Action noneback = null) {
            await this._SelectList.ExecuteOne(cursor, noneback);
        }

        public async Task ExecuteMany(Action<IExecutedQuery<TEntity>> cursor, Action noneback = null) {
            await this._SelectList.ExecuteMany(cursor, noneback);
        }

    }

}
