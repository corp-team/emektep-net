﻿

using HDS.App.Domain.Data;
using HDS.App.Domain.Enums;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Static;

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace HDS.App.Domain.Query {

    class QueryBuilder : IQueryBuilder {


        private StringBuilder _QueryString;
        private StringBuilder _FieldsString;
        private StringBuilder _ConstraintString;
        private StringBuilder _OrderByString;
        private string paramKey = "";
        private string nameFormatter = "";

        public IDOSchemaBuilder SchemaBuilder { get; set; }
        public IDataEngine Engine { get; } = SDbParams.DataTools.GenerateEngine();

        private string FieldNameFromProperty(MemberInfo prop) {

            return "{0}, ".Puts(FieldNameFromPropertyPlain(prop));

        }
        private string FieldNameFromPropertyPlain(MemberInfo prop) {

            return "{0}.{1}".Puts(this.SchemaBuilder.GetFormatted(), nameFormatter.Puts(prop.Name));

        }

        public List<MemberInfo> Fields { get; private set; }

        public QueryBuilder(IDOSchemaBuilder schemaBuilder) {

            _QueryString = new StringBuilder("SELECT #fields# FROM #table# #where# #orderby#");
            _FieldsString = new StringBuilder("*");
            _ConstraintString = new StringBuilder("WHERE 1=1 ");
            _OrderByString = new StringBuilder("ORDER BY ");
            Fields = new List<MemberInfo>();
            this.SchemaBuilder = schemaBuilder;
            switch (SDbParams.CurrentServerType) {
                case EServerType.MySql:
                    paramKey = "?";
                    nameFormatter = "`{0}`";
                    break;
                case EServerType.MsSql:
                    paramKey = "@";
                    nameFormatter = "[{0}]";
                    break;
                default:
                    throw new ArgumentException("Unsupported Server Type");
            }

        }

        public IQueryBuilder AddField(MemberInfo prop) {

            if (_FieldsString.ToString().Equals("*")) {
                _FieldsString.Clear();
                Fields.Clear();
            }
            if (!Fields.Contains(prop)) {
                _FieldsString.Append(FieldNameFromProperty(prop));
                Fields.Add(prop);
            }
            return this;

        }

        public IQueryBuilder RemoveField<TEntity>(MemberInfo prop)
            where TEntity : DOBase<TEntity> {

            if (Fields.Count == 0) {
                Fields.AddRange(Activator.CreateInstance<TEntity>().TakeFields(EResolveBy.AllAssigned));
            }
            if (Fields.Contains(prop)) {
                _FieldsString.Replace(FieldNameFromProperty(prop), "");
                Fields.Remove(prop);
            }
            if (_FieldsString.ToString().Equals("")) {
                _FieldsString.Append("*");
            }
            return this;

        }

        public IQueryBuilder AndConstraint(MemberInfo prop, object value, ExpressionType nodeType) {
            _ConstraintString.AppendFormat(" AND {0}{2}{3}{1}", "{0}.{1}".Puts(this.SchemaBuilder.GetFormatted(), nameFormatter.Puts(prop.Name)),
                prop.Name, SDbParams.DataTools.SqlExpression(nodeType), paramKey);
            Engine.AddWithValue($"{paramKey}{prop.Name}", value);
            return this;

        }

        public IQueryBuilder OrConstraint(MemberInfo prop, object value, ExpressionType nodeType) {
            _ConstraintString.AppendFormat(" OR {0}{2}{3}{1}", "{0}.{1}".Puts(this.SchemaBuilder.GetFormatted(), nameFormatter.Puts(prop.Name)),
                prop.Name, SDbParams.DataTools.SqlExpression(nodeType), paramKey);
            Engine.AddWithValue($"{paramKey}{prop.Name}", value);
            return this;
        }

        public IQueryBuilder OrderByField(MemberInfo prop, EOrderBy orderBy) {

            _OrderByString.AppendFormat("{0} {1}, ", FieldNameFromPropertyPlain(prop), orderBy.GetDescription());
            return this;

        }

        public StringBuilder BuildQuery() {

            _QueryString.Replace("#fields#", _FieldsString.ToString().TrimEnd(' ', ','))
                .Replace("#table#", this.SchemaBuilder.GetFormatted())
                .Replace("#where#", _ConstraintString.ToString() == "WHERE 1=1 " ? "" : _ConstraintString.ToString().TrimEnd(' ', ','))
                .Replace("#orderby#", _OrderByString.ToString() == "ORDER BY " ? "" : _OrderByString.ToString().TrimEnd(' ', ','));
            return _QueryString;
        }
    }

}