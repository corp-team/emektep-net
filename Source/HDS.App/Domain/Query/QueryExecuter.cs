﻿using HDS.App.Domain.Enums;
using HDS.App.Domain.Objects;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HDS.App.Domain.Query {

    public class QueryExecuter<TEntity> : IQueryExecuter<TEntity>
         where TEntity : DOBase<TEntity> {

        private SelectList<TEntity> _SelectList;

        public QueryExecuter(SelectList<TEntity> selectList) {
            _SelectList = selectList;
            if (_SelectList.AllColumns) {
                _SelectList.QueryBuilder.Fields.AddRange(
                    Activator.CreateInstance<TEntity>().TakeFields(EResolveBy.All));
            }
        }

        public async Task<IEnumerable<IExecutedQuery<TEntity>>> GenerateExecutedQueryList(Action noneback = null) {
            var list = new List<IExecutedQuery<TEntity>>();
            var commandText = _SelectList.QueryBuilder.BuildQuery().ToString();
            using (var command = _SelectList.QueryBuilder.Engine.ConnectifiedCommand(commandText)) {
                using (var reader = await _SelectList.QueryBuilder.Engine.ReadCommand(command)) {
                    var c = 0;
                    if (reader.Read()) {
                        list.Add(new ExecutedQuery<TEntity>(reader,
                                _SelectList.QueryBuilder.Fields, c++));
                        while (reader.Read()) {
                            list.Add(new ExecutedQuery<TEntity>(reader,
                                _SelectList.QueryBuilder.Fields, c++));
                        }
                    } else {
                        noneback?.Invoke();
                    }

                }
            }
            return list.ToArray();
        }

        public async Task GenerateSingleExecutedQuery(Action<ExecutedQuery<TEntity>> doneback, Action noneback = null) {
            var commandText = _SelectList.QueryBuilder.BuildQuery().ToString();
            using (var command = _SelectList.QueryBuilder.Engine.ConnectifiedCommand(commandText)) {
                using (var reader = await _SelectList.QueryBuilder.Engine.ReadCommand(command)) {
                    if (reader.Read()) {
                        doneback(new ExecutedQuery<TEntity>(reader,
                            _SelectList.QueryBuilder.Fields, 0));
                    } else {
                        noneback?.Invoke();
                    }
                }
            }
        }
    }

}