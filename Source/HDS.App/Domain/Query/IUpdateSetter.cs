﻿
using HDS.App.Domain.Objects;

namespace HDS.App.Domain.Query {

    public interface IUpdateSetter<TEntity>
        where TEntity : DOBase<TEntity> {

        IUpdateClause<TEntity> Set<TProp>(TProp value);

    }

}