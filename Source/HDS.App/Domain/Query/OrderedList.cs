﻿using HDS.App.Domain.Enums;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Static;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace HDS.App.Domain.Query {

    public class OrderedList<TEntity> : IOrderedList<TEntity>
        where TEntity : DOBase<TEntity> {

        private SelectList<TEntity> _SelectList;

        public OrderedList(SelectList<TEntity> selectList) {
            this._SelectList = selectList;
        }

        public IOrderedList<TEntity> By<TProp>(Expression<Func<TEntity, TProp>> selector, EOrderBy orderBy) {

            var param = selector.Parameters[0] as ParameterExpression;
            var prop = selector.ResolveMember();
            this._SelectList.QueryBuilder.OrderByField(prop, orderBy);
            return this;

        }

        public async Task ExecuteOne(Action<IExecutedQuery<TEntity>> cursor, Action fallback = null) {
            await this._SelectList.ExecuteOne(cursor, fallback);
        }

        public async Task ExecuteMany(Action<IExecutedQuery<TEntity>> cursor, Action noneback = null) {
            await this._SelectList.ExecuteMany(cursor, noneback);
        }

    }

}