﻿using HDS.App.Domain.Objects;
using HDS.App.Extensions.Static;
using System.Linq;
using System.Reflection;

namespace HDS.App.Domain.Query {

    internal class UpdateSetter<TEntity> : IUpdateSetter<TEntity>
        where TEntity : DOBase<TEntity> {

        private MemberInfo prop;
        private IExecutedQuery executedQuery;

        public UpdateSetter(IExecutedQuery executedQuery, MemberInfo prop) {
            this.executedQuery = executedQuery;
            this.prop = prop;
        }

        public IUpdateClause<TEntity> Set<TProp>(TProp value) {

            if (value != null && !value.Equals(default(TProp))) {
                this.prop.SetValue(this.executedQuery.Entities.Single(e => e.IsTypeOf<TEntity>()), value);
                this.executedQuery.AddUpdateParameter<TEntity>(this.prop, value);
            }
            return new UpdateClause<TEntity>(this.executedQuery);

        }
    }

}