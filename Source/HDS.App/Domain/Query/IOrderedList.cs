﻿using HDS.App.Domain.Enums;
using HDS.App.Domain.Objects;
using System;
using System.Linq.Expressions;

namespace HDS.App.Domain.Query {

    public interface IOrderedList<TEntity> : ICursorExecuter<TEntity>
        where TEntity : DOBase<TEntity> {

        IOrderedList<TEntity> By<TProp>(Expression<Func<TEntity, TProp>> selector, EOrderBy orderBy);

    }

}
