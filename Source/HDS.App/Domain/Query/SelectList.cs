﻿using HDS.App.Domain.Objects;
using HDS.App.Extensions.Static;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace HDS.App.Domain.Query {

    public class SelectList<TEntity> : ISelectList<TEntity>
        where TEntity : DOBase<TEntity> {

        private readonly IQueryBuilder _QueryBuilder;
        private IConfinedList<TEntity> _ConfinedList;
        private IOrderedList<TEntity> _OrderedList;

        private QueryExecuter<TEntity> _QueryExecuter;
        private bool allColumns;

        internal IQueryBuilder QueryBuilder {
            get {
                return _QueryBuilder;
            }
        }

        public bool AllColumns {
            get {
                return allColumns;
            }
        }

        public SelectList(bool allColumns = false) {
            this.allColumns = allColumns;
            this._QueryBuilder = new QueryBuilder((Activator.CreateInstance<TEntity>() as IDOBase).SchemaBuilder);
        }

        public ISelectList<TEntity> Add<TProp>(Expression<Func<TEntity, TProp>> columnSelector) {

            var prop = columnSelector.ResolveMember();
            this.QueryBuilder.AddField(prop);
            return this;

        }

        public ISelectList<TEntity> Remove<TProp>(Expression<Func<TEntity, TProp>> columnSelector) {

            var prop = columnSelector.ResolveMember();
            this.QueryBuilder.RemoveField<TEntity>(prop);
            return this;

        }

        public IConfinedList<TEntity> Where(Expression<Func<TEntity, bool>> columnSelector) {

            return (_ConfinedList = _ConfinedList ?? new ConfinedList<TEntity>(this, columnSelector));

        }

        public IOrderedList<TEntity> Order() {

            return (_OrderedList = _OrderedList ?? new OrderedList<TEntity>(this));

        }

        public async Task ExecuteOne(Action<IExecutedQuery<TEntity>> cursor, Action noneback = null) {

            _QueryExecuter = _QueryExecuter ?? new QueryExecuter<TEntity>(this);
            await _QueryExecuter.GenerateSingleExecutedQuery(cursor, noneback);

        }

        public async Task ExecuteMany(Action<IExecutedQuery<TEntity>> cursor, Action noneback = null) {
            _QueryExecuter = _QueryExecuter ?? new QueryExecuter<TEntity>(this);
            foreach (var eq in await _QueryExecuter.GenerateExecutedQueryList(noneback)) {
                cursor(eq);
            }
        }

    }

}