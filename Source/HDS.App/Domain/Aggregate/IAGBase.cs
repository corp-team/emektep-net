﻿
using HDS.App.Domain.Objects;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace HDS.App.Domain.Aggregate {

    public interface IAGBase : IDomainBaseModel {

        long IDProperty { get; }
        IAGBase Drop();
        void BuildView();
        IAGSchemaBuilder GetSchemaBuilder();
        TDomain GetMappedDomain<TDomain>()
            where TDomain : DOBase<TDomain>;
        IDOBase GetMappedDomainByType(Type domainType);
        bool IsColumnRequired(PropertyInfo property);
        IEnumerable<(MemberInfo domain, MemberInfo aggragate)> Mappings { get; }
    }

}