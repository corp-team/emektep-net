﻿using System;
using System.Threading.Tasks;

namespace HDS.App.Domain.Aggregate {

    public interface IConfinedAggragator<T>
        where T : AGBase<T> {

        Task ExecuteList(Action<T> cursor, Action<Exception> failback = null);
        Task ExecuteSingle(Action<T> cursor, Action<Exception> failback = null);

    }

}