﻿
using HDS.App.Domain.Data;
using HDS.App.Extensions.Decorators;
using HDS.App.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace HDS.App.Domain.Aggregate {

    internal class Aggragator<T> : IAggragator<T>, ISelectedAggragator<T>, IWheredAggragator<T>, IConfinedAggragator<T>
        where T : AGBase<T> {

        private List<Expression> _SelectList = new List<Expression>();
        private Expression _DistinctColumn;
        private Queue<KeyValuePair<ExpressionType, BinaryExpression>> _WhereQueue =
            new Queue<KeyValuePair<ExpressionType, BinaryExpression>>();

        public Aggragator() {

            DataTools = SDbParams.DataTools;

        }

        private IDataTools DataTools;

        public ISelectedAggragator<T> SelectColumns<TProp>(
             Expression<Func<T, TProp>> column) {

            _SelectList.Add(column.Body);
            return this;

        }

        public ISelectedAggragator<T> SelectAllColumns() {

            _SelectList.Clear();
            return this;

        }

        public IAggragator<T> SetDistinctColumn<TProp>(Expression<Func<T, TProp>> column) {

            _DistinctColumn = column.Body;
            return this;

        }

        public IWheredAggragator<T> Where(Expression<Func<T, bool>> selector) {

            _WhereQueue.Enqueue(new KeyValuePair<ExpressionType, BinaryExpression>(ExpressionType.And, selector.Body as BinaryExpression));
            return this;

        }

        public IConfinedAggragator<T> AndAlso(Expression<Func<T, bool>> selector) {

            _WhereQueue.Enqueue(new KeyValuePair<ExpressionType, BinaryExpression>(ExpressionType.AndAlso, selector.Body as BinaryExpression));
            return this;

        }

        public IConfinedAggragator<T> OrElse(Expression<Func<T, bool>> selector) {

            _WhereQueue.Enqueue(new KeyValuePair<ExpressionType, BinaryExpression>(ExpressionType.OrElse, selector.Body as BinaryExpression));
            return this;

        }

        public async Task ExecuteList(Action<T> cursor, Action<Exception> failback = null) {

            try {
                var commandText = this.ToString();
                using (var engine = DataTools.GenerateEngine()) {
                    using (var command = engine.ConnectifiedCommand(commandText)) {
                        using (var reader = await engine.ReadCommand(command)) {
                            while (reader.Read()) {
                                var et = ExtractFrom(reader);
                                cursor(et);
                            }
                        }
                        command.Connection.Close();
                    }
                }
            } catch (Exception ex) {
                failback?.Invoke(ex);
            }

        }

        public async Task ExecuteSingle(Action<T> cursor, Action<Exception> failback = null) {

            try {
                var commandText = this.ToString();
                using (var engine = DataTools.GenerateEngine()) {
                    using (var command = engine.ConnectifiedCommand(commandText)) {
                        using (var reader = await engine.ReadCommand(command)) {
                            if (reader.Read()) {
                                var et = ExtractFrom(reader);
                                cursor(et);
                            }
                        }
                        command.Connection.Close();
                    }
                }
            } catch (Exception ex) {
                failback?.Invoke(ex);
            }

        }

        public override string ToString() {

            var entitySchemaBuilder =
                (Activator.CreateInstance(typeof(T)) as IAGBase).GetSchemaBuilder();
            var selectList = "{0}.*".Puts(entitySchemaBuilder.GetFormatted());
            var viewAndSchema = entitySchemaBuilder.GetFormatted();
            if (_SelectList.Count > 0) {
                if (_DistinctColumn == null) {
                    selectList = _SelectList.Select(se => "{0}.[{1}]".Puts(
                        viewAndSchema, se.ExposeMember().Name))
                        .Aggregate((p, n) => p + ", " + n);
                } else {
                    selectList = _SelectList.Where(se => se.ExposeMember().Name != _DistinctColumn.ExposeMember().Name)
                        .Select(se => "{0}.[{1}]".Puts(viewAndSchema,
                            se.ExposeMember().Name))
                        .Aggregate((p, n) => p + ", " + n);
                    selectList.Insert(0, "DISTINCT {0}.[{1}], ".Puts(viewAndSchema,
                            _DistinctColumn.ExposeMember().Name));
                }
            } else {
                if (_DistinctColumn != null) {
                    selectList = selectList.Insert(0, "DISTINCT {0}.[{1}], ".Puts(viewAndSchema, _DistinctColumn.ExposeMember().Name));
                }
            }
            var str = new StringBuilder("SELECT {0} FROM {1} WHERE 1=1".Puts(selectList, viewAndSchema));

            Action<BinaryExpression, StringBuilder> reducer = null;
            reducer = (be, sb) => {
                if (be.Left is BinaryExpression lbinary) {
                    reducer(lbinary, sb);
                    str.AppendFormat(" {0} ", be.NodeTypeString() == "&&" ? "AND" : "OR");
                } else if (be.Left is UnaryExpression lunary) {
                    str.AppendFormat("{0}.{1}", entitySchemaBuilder.GetFormatted(),
                        SDbParams.NameFormatter.Puts(lunary.ExposeMember().Name));
                    str.AppendFormat(" {0} ", be.NodeTypeString());
                } else if (be.Left is MemberExpression lmember) {
                    var prop = lmember.ExposeMember();
                    if (lmember.ExposeMember().Name == "IDProperty") {
                        var props = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public)
                        .Where(p => p.GetCustomAttribute<IDPropertyLocatorAttribute>() != null);
                        if (props.Count() > 1) {
                            throw new InvalidOperationException("Multiple IDPropertyLocator Specified");
                        } else if (props.Count() == 0) {
                            throw new InvalidOperationException("None of Multiple IDPropertyLocator Specified");
                        }
                        prop = props.First();
                    } 
                    str.AppendFormat("{0}.{1}", entitySchemaBuilder.GetFormatted(),
                        SDbParams.NameFormatter.Puts(prop.Name));
                    str.AppendFormat(" {0} ", be.NodeTypeString());
                    str.AppendFormat("{0}", lmember.Resultify());
                }
                if (be.Right is BinaryExpression rbinary) {
                    reducer(rbinary, sb);
                } else {
                    str.AppendFormat("{0}", be.Right.Resultify());
                }
            };

            while (_WhereQueue.Count > 0) {
                var toq = _WhereQueue.Dequeue();
                var operand = "AND";
                if (toq.Key == ExpressionType.OrElse) {
                    operand = "OR";
                }
                str.AppendFormat(" {0} ", operand);
                reducer(toq.Value, str);
            }
            return str.ToString();

        }

        private static T ExtractFrom(System.Data.IDataReader reader) {
            var et = Activator.CreateInstance<T>() as T;
            foreach (var p in typeof(T).GetProperties()) {
                if (reader.HasColumn(p.Name)) {
                    if (!p.HasAttribute<ComputedFieldAttribute>()) {
                        var value = reader[p.Name];
                        if (!value.GetType().Equals(typeof(System.DBNull))) {
                            if (p.PropertyType.IsEnum) {
                                p.SetValue(et, Enum.Parse(p.PropertyType, value.ToString()));
                            } else if (p.PropertyType.IsConvertible()) {
                                p.SetValue(et, Convert.ChangeType(value, p.PropertyType));
                            } else {
                                p.SetValue(et, reader[p.Name]);
                            }
                        }
                    }
                }
            }

            return et;
        }

    }

}