﻿using HDS.App.Domain.Objects;
using System;
using System.Text;

namespace HDS.App.Domain.Aggregate {

    public interface IAGViewBuilder<T> where T : AGBase<T> {

        IAGMappedViewBuilder<T> MapsTo(Action<IAGSchemaBuilder> schematizer);
        IAGSchemaBuilder SchemaBuilder { get; }
        StringBuilder QueryString { get; }

    }

}