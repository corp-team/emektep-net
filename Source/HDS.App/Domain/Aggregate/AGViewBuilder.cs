﻿using HDS.App.Domain.Enums;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Static;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace HDS.App.Domain.Aggregate {

    public class AGViewBuilder<TAggregate> : IAGViewBuilder<TAggregate>, IAGMappedViewBuilder<TAggregate>, IAGSelectedViewBuilder<TAggregate>
        where TAggregate : AGBase<TAggregate> {

        public AGViewBuilder() {
            SchemaBuilder = new AGSchemaBuilder();
            QueryString = new StringBuilder("SELECT #selectors# FROM #table# #joinlist#");
        }

        internal void AppendJoinString(string join) {
            QueryString.Replace("#joinlist#", "{0} #joinlist#".Puts(join));
        }

        public IAGSchemaBuilder SchemaBuilder { get; private set; }
        public StringBuilder QueryString { get; private set; }
        internal List<(MemberInfo domain, MemberInfo aggragate)> Mappings { get; private set; } = new List<(MemberInfo domain, MemberInfo aggragate)>();

        public IAGMappedViewBuilder<TAggregate> MapsTo(Action<IAGSchemaBuilder> schematizer) {

            schematizer(this.SchemaBuilder);
            return this;

        }

        internal IDOBase GetMappedDomain(Type domainType, TAggregate agg) {
            var obj = Activator.CreateInstance(domainType) as IDOBase;
            foreach (var (domain, aggregate) in Mappings) {
                if (domain.ReflectedType.IsAssignableFrom(domainType)) {
                    domain.SetValue(obj, aggregate.GetValue(agg));
                }
            }
            return obj;
        }

        internal bool IsColumnRequired(PropertyInfo property) {
            var (domain, aggregate) = Mappings.SingleOrDefault(mp => mp.Item2.Name == property.Name);
            if (domain != null) {
                return (Activator.CreateInstance(domain.ReflectedType) as IDOBase).TakeFields(EResolveBy.AllRequired)
                    .Any(f => f.Name == domain.Name);
            } else {
                return false;
            }
        }

        public IAGSelectedViewBuilder<TAggregate> Select<TDomain>(Action<IAGSelectList<TDomain, TAggregate>> selectCursor = null)
            where TDomain : DOBase<TDomain> {
            var entity = Activator.CreateInstance<TDomain>();
            ApplyCursor(selectCursor);
            QueryString.Replace("#table#", "{0}".Puts(entity.SchemaBuilder.GetFormatted()));
            return this;
        }

        public IJoinBuilder<TDomain, TAggregate> InnerJoin<TDomain>(Action<IAGSelectList<TDomain, TAggregate>> selectCursor = null)
            where TDomain : DOBase<TDomain> {
            ApplyCursor(selectCursor);
            return new JoinBuilder<TDomain, TAggregate>(this);
        }

        public IJoinBuilder<TDomain, TAggregate> OuterJoin<TDomain>(Action<IAGSelectList<TDomain, TAggregate>> selectCursor = null)
            where TDomain : DOBase<TDomain> {
            ApplyCursor(selectCursor);
            return new JoinBuilder<TDomain, TAggregate>(this, true);
        }

        private void ApplyCursor<TDomain>(Action<IAGSelectList<TDomain, TAggregate>> selectCursor) where TDomain : DOBase<TDomain> {
            if (selectCursor != null) {
                var entity = Activator.CreateInstance<TDomain>() as IDOBase;
                IAGSelectList<TDomain, TAggregate> selectList = new AGSelectList<TDomain, TAggregate>();
                selectCursor(selectList);
                QueryString.Replace("#selectors#", "{0}, #selectors#".Puts(selectList));
                var mappings = (selectList as AGSelectList<TDomain, TAggregate>).SelectedExpressions
                    .Select(colalias => (domain: colalias.Item1.ExposeMember(), aggragate: colalias.Item2.ExposeMember()));
                Mappings.AddRange(mappings);
            }
        }

    }

}