﻿
using HDS.App.Domain.Data;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Decorators;
using HDS.App.Extensions.Static;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace HDS.App.Domain.Aggregate {

    public abstract class AGBase<TAggregate> : IAGBase
        where TAggregate : AGBase<TAggregate> {

        public AGBase() {
            _Builder = GetBuilder();
        }

        private IAGViewBuilder<TAggregate> _Builder;
        private IAGViewBuilder<TAggregate> GetBuilder() {
            IAGViewBuilder<TAggregate> builder = new AGViewBuilder<TAggregate>();
            Map(builder);
            return builder;
        }

        protected abstract void Map(IAGViewBuilder<TAggregate> builder);

        [TableColumn(Order = 0)]
        public long IDProperty {
            get {
                var props = typeof(TAggregate).GetProperties(BindingFlags.Instance | BindingFlags.Public)
                        .Where(p => p.GetCustomAttribute<IDPropertyLocatorAttribute>() != null);
                if (props.Count() > 1) {
                    throw new InvalidOperationException("Multiple IDPropertyLocator Specified");
                } else if (props.Count() == 0) {
                    throw new InvalidOperationException("None of Multiple IDPropertyLocator Specified");
                }
                var prop = props.First();
                return Convert.ToInt64(prop.GetValue(this));
            }
        }

        public TDomain GetMappedDomain<TDomain>()
            where TDomain : DOBase<TDomain> {
            return (_Builder as AGViewBuilder<TAggregate>).GetMappedDomain(typeof(TDomain), this as TAggregate) as TDomain;
        }

        public IDOBase GetMappedDomainByType(Type domainType) {
            if (domainType.IsSubclassOf(typeof(DOBase<>).MakeGenericType(domainType))) {
                return (_Builder as AGViewBuilder<TAggregate>).GetMappedDomain(domainType, this as TAggregate);
            } else {
                throw new ArgumentException($"{domainType.ToString()} Should Have Been Assignable From DOBase<>");
            }
        }
        public bool IsColumnRequired(PropertyInfo property) {
            return (_Builder as AGViewBuilder<TAggregate>).IsColumnRequired(property);
        }
        public IAGSchemaBuilder GetSchemaBuilder() {

            return _Builder.SchemaBuilder;

        }

        public IAGBase Drop() {
            using (var engine = SDbParams.DataTools.GenerateEngine()) {
                var commandText = "";
                switch (SDbParams.CurrentServerType) {
                    case Data.EServerType.MySql:
                        commandText = "DROP VIEW IF EXISTS {1}";
                        break;
                    case Data.EServerType.Unset:
                        commandText = "IF EXISTS(select * FROM sys.views where name = '{0}') DROP VIEW {1}";
                        break;
                    default:
                        throw new ArgumentException("Unsupported Server Type");
                }
                using (var command = engine.ConnectifiedCommand(commandText.Puts(_Builder.SchemaBuilder.GetViewName(),
                        _Builder.SchemaBuilder.GetFormatted()))) {
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
            }
            return this;

        }

        public void BuildView() {
            var viewAndSchema = _Builder.SchemaBuilder.Build();
            var selectStatement = _Builder.QueryString
                .Replace(", #selectors#", "")
                .Replace(" #joinlist#", "");
            var commandText = "";
            switch (SDbParams.CurrentServerType) {
                case Data.EServerType.MySql:
                    commandText = "CREATE VIEW {0} AS {1}".Puts(viewAndSchema, selectStatement);
                    break;
                case Data.EServerType.Unset:
                    var countStatement = Regex.Replace(selectStatement.ToString(), @"(SELECT )(.*)( FROM.*)", "$1COUNT(*)$3");
                    var createViewStatement = selectStatement.Insert(selectStatement.ToString().ToUpper().IndexOf("SELECT") + 7, "TOP ({0})".Puts(countStatement));
                    commandText = "CREATE VIEW {0} WITH SCHEMABINDING AS {1}".Puts(viewAndSchema, createViewStatement);
                    break;
                default:
                    throw new ArgumentException("Unsupported Server Type");
            }

            using (var engine = SDbParams.DataTools.GenerateEngine()) {
                using (var command = engine.ConnectifiedCommand(commandText)) {
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                }
            }
        }

        public string Slug {
            get {
                return _Builder.SchemaBuilder.GetViewName().ToLower();
            }
        }

        [JsonIgnore]
        public IEnumerable<(MemberInfo domain, MemberInfo aggragate)> Mappings {
            get {
                return (_Builder as AGViewBuilder<TAggregate>).Mappings.ToArray();
            }
        }

    }

}