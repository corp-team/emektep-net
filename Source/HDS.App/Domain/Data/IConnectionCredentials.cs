﻿namespace HDS.App.Domain.Data {

    public interface IConnectionCredentials {

        string ConnectionString();

    }

}
