﻿using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace HDS.App.Domain.Data {

    public interface IDataTools {

        string SqlExpression(ExpressionType nodeType);

        IDataEngine GenerateEngine();

        string ParseFields(IEnumerable<MemberInfo> fields);
        string ParseParams(IEnumerable<MemberInfo> fields);

    }

}
