﻿using HDS.App.Domain.Enums;
using HDS.App.Domain.Objects;
using HDS.App.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace HDS.App.Domain.Data {
    public static class SDbParams {

        public static bool LocalMachine { get; } = new StackTrace().GetReferencingAssembly().IsDebugBuild();


        internal static IDataTools DataTools { get; } = new DataTools();
        internal static IDOTableBuilder<TEntity> TableBuilder<TEntity>() where TEntity : DOBase<TEntity> {
            return new DOTableBuilder<TEntity>();
        }

        public static string ConnectionString() {

            if (LocalMachine) {
                return DebugConnectionString[CurrentServerType];

            } else {
                return ReleaseConnectionString[CurrentServerType];
            }

        }

        private static EServerType _CurrentServerType = EServerType.Unset;

        public static EServerType CurrentServerType {
            get {
                return _CurrentServerType;
            }
        }


        public static string ParamKey {
            get {
                switch (CurrentServerType) {
                    case EServerType.MySql:
                        return "?";
                    case EServerType.MsSql:
                        return "@";
                    default:
                        throw new NotSupportedException($"ServerKey: {CurrentServerType.ToString()} Not Supported");
                }
            }
        }

        public static string NameFormatter {
            get {
                switch (CurrentServerType) {
                    case EServerType.MySql:
                        return "`{0}`";
                    case EServerType.MsSql:
                        return "[{0}]";
                    default:
                        throw new NotSupportedException($"ServerKey: {CurrentServerType.ToString()} Not Supported");
                }
            }
        }

        private static Dictionary<EServerType, String> DebugConnectionString = new Dictionary<EServerType, string>();
        private static Dictionary<EServerType, String> ReleaseConnectionString = new Dictionary<EServerType, string>();

        public static void SetConnectionString(string connectionString, EConnectionStringMode mode, EServerType serverType = EServerType.Unset) {
            _CurrentServerType = serverType;
            switch (serverType) {
                case EServerType.MySql:
                    connectionString += ";Allow User Variables=True";
                    break;
                case EServerType.MsSql:
                    break;
                default:
                    break;
            }
            switch (mode) {
                case EConnectionStringMode.Debug:
                    DebugConnectionString[serverType] = connectionString;
                    break;
                case EConnectionStringMode.Release:
                    ReleaseConnectionString[serverType] = connectionString;
                    break;
                default:
                    break;
            }
        }

    }
}
