﻿
using System;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace HDS.App.Domain.Data {

    public interface IDataEngine : IDisposable {

        IDbCommand ConnectifiedCommand(string commandText = "");
        IDbCommand IdentityCommand(IDbCommand parentCommand);

        IDataEngine AddWithValue(string name, object value);
        IDataEngine AddParameter(string name, Type type);

        Task<int> RunCommand(IDbCommand command);
        Task<DbDataReader> ReadCommand(IDbCommand command);
    }

}