﻿using HDS.App.Domain.Aggregate;
using HDS.App.Domain.Contracts;
using HDS.App.Domain.DML;
using HDS.App.Domain.Enums;
using HDS.App.Domain.Objects;
using HDS.App.Domain.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace HDS.App.Engines.Core {
    internal class DOSyncEngine<TEntity>
        where TEntity : DOBase<TEntity> {

        protected readonly Func<Exception, DMLResponse<TEntity>> _Fallback;

        public DOSyncEngine(Func<Exception, DMLResponse<TEntity>> fallback) {
            _Fallback = fallback;
        }
        public DOSyncEngine() {
            _Fallback = (exc) => new DMLResponse<TEntity>() {
                Fault = exc,
                Success = false
            };
        }
        public DMLResponse<TEntity> DeleteSync<TKey>(TKey key) {

            DMLResponse<TEntity> response = new DMLResponse<TEntity>() { Success = false };
            Q<TEntity>.SelectAllColumns().Where(x => x.ID == 1).ExecuteOne(xq => {
                response = xq.Delete();
            }, () => response = new DMLResponse<TEntity>() {
                Fault = new Exception($"Entity ID: {key} not found"),
                Success = false
            }).Wait();
            return response;

        }

        public DMLResponse<TEntity> InsertSync(TEntity entity) {

            return CR.Single(entity).UpsertSync();

        }

        public DMLResponse<TEntity> InsertIfAbsentSync<TProp>(TEntity entity, Func<TEntity, TProp> discriminator) {
            var prop = discriminator(entity);
            var response = SelectBySync(e => (object)discriminator(e) == (object)prop);
            var discriminated = response.CollectionResponse.FirstOrDefault();
            if (discriminated == null) {
                return CR.Single(entity).UpsertSync();
            }
            return new DMLResponse<TEntity>() { SingleResponse = discriminated ?? entity, Success = true };
        }

        public DMLResponse<TEntity> UpdateSync(long id, Func<IUpdateClause<TEntity>, IUpdateClause<TEntity>> updater) {

            DMLResponse<TEntity> response = new DMLResponse<TEntity>() { Success = false };
            Q<TEntity>.SelectAllColumns().Where(u => u.ID == id).ExecuteOne(cursor => {
                response = updater(cursor.UpdateClause()).PersistUpdate() as DMLResponse<TEntity>;
            }, () => response = new DMLResponse<TEntity>() {
                Fault = new Exception($"Entity with ID: {id} not found"),
                Success = false
            }).Wait();
            return response;

        }

        public IDMLResponse UpdateAggregateSync(IAGBase aggregate) {

            IDMLResponse response = new DMLResponse<TEntity>() { Success = false };
            Q<TEntity>.SelectAllColumns().Where(u => u.ID == aggregate.IDProperty).ExecuteOne(cursor => {
                response = cursor.UpdateClause().UpsertAggregate(aggregate);
            }, () => response = new DMLResponse<TEntity>() {
                Fault = new Exception($"Entity with ID: {aggregate.IDProperty} not found"),
                Success = false
            }).Wait();
            return response;

        }

        public DMLResponse<TEntity> SelectAllSync() {
            DMLResponse<TEntity> response = new DMLResponse<TEntity>() { Success = false };
            var list = new List<TEntity>();
            Q<TEntity>.SelectAllColumns().ExecuteMany((query) => {
                list.Add(query.ResolvedEntity);
            });
            response = new DMLResponse<TEntity> { CollectionResponse = list };
            return response;
        }

        public DMLResponse<TEntity> SelectBySync(params Expression<Func<TEntity, bool>>[] selectors) {
            DMLResponse<TEntity> response = new DMLResponse<TEntity>() { Success = false };
            var list = new List<TEntity>();
            var confinement = Q<TEntity>.SelectAllColumns().Where(selectors.First());
            selectors.Skip(1).ToList().ForEach(sel => confinement = confinement.And(sel));
            confinement.ExecuteMany((query) => {
                list.Add(query.ResolvedEntity);
            }).Wait();
            response = new DMLResponse<TEntity> { CollectionResponse = list };
            return response;
        }

        public DMLResponse<TEntity> SelectSingleSync(Expression<Func<TEntity, bool>> selector) {
            DMLResponse<TEntity> response = new DMLResponse<TEntity>() { Success = false };
            Q<TEntity>.SelectAllColumns().Where(selector).ExecuteOne((query) => {
                response = new DMLResponse<TEntity> { SingleResponse = query.ResolvedEntity };
            }).Wait();
            return response;
        }
        public DMLResponse<TEntity> SelectLastRecordSync<TKey>(Expression<Func<TEntity, TKey>> keySelector) {
            DMLResponse<TEntity> response = new DMLResponse<TEntity>() { Success = false };
            Q<TEntity>.SelectAllColumns().Order().By(keySelector, EOrderBy.ASC).ExecuteMany((query) => {
                response = new DMLResponse<TEntity> { SingleResponse = query.ResolvedEntity };
            }).Wait();
            return response;
        }

        public DMLResponse<TEntity> SelectSingleByIDSync<TKey>(TKey key) {
            DMLResponse<TEntity> response = new DMLResponse<TEntity>() { Success = false };
            Q<TEntity>.SelectAllColumns().Where(e => (object)e.ID == (object)key).ExecuteOne((query) => {
                response = new DMLResponse<TEntity> { SingleResponse = query.ResolvedEntity };
            }).Wait();
            return response;
        }

    }
}
