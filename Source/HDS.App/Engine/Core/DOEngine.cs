﻿using HDS.App.Domain.Aggregate;
using HDS.App.Domain.Contracts;
using HDS.App.Domain.DML;
using HDS.App.Domain.Enums;
using HDS.App.Domain.Objects;
using HDS.App.Domain.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace HDS.App.Engines.Core {
    internal class DOEngine<TEntity> : DOSyncEngine<TEntity>, IDOEngine<TEntity>, IGenericEngine
        where TEntity : DOBase<TEntity> {

        public DOEngine(Func<Exception, DMLResponse<TEntity>> fallback)
            : base(fallback) {

        }
        public DOEngine() : base() {

        }
        public async Task<DMLResponse<TEntity>> Delete<TKey>(TKey key) {
            DMLResponse<TEntity> response = new DMLResponse<TEntity>() { Success = false };
            await Q<TEntity>.SelectAllColumns().Where(x => x.ID == 1).ExecuteOne(xq => {
                response = xq.Delete();
            }, () => response = new DMLResponse<TEntity>() {
                Fault = new Exception($"Entity ID: {key} not found"),
                Success = false
            });
            return response;
        }
        public async Task<DMLResponse<TEntity>> Insert(TEntity entity) {
            return await CR.Single(entity).Upsert();
        }
        public async Task<DMLResponse<TEntity>> InsertIfAbsent<TProp>(TEntity entity, Func<TEntity, TProp> discriminator) {
            var prop = discriminator(entity);
            var response = await SelectBy(e => (object)discriminator(e) == (object)prop);
            var discriminated = response.CollectionResponse.FirstOrDefault();
            if (discriminated == null) {
                return await CR.Single(entity).Upsert();
            }
            return new DMLResponse<TEntity>() { SingleResponse = discriminated ?? entity, Success = true };
        }
        public async Task<DMLResponse<TEntity>> Update(long id, Func<IUpdateClause<TEntity>, IUpdateClause<TEntity>> updater) {
            DMLResponse<TEntity> result = new DMLResponse<TEntity>() { Success = false };
            await Q<TEntity>.SelectAllColumns().Where(u => u.ID == id).ExecuteOne(cursor => {
                result = updater(cursor.UpdateClause()).PersistUpdate() as DMLResponse<TEntity>;
            }, () => result = new DMLResponse<TEntity>() {
                Fault = new Exception($"Entity with ID: {id} not found"),
                Success = false
            });
            return result;
        }
        public async Task<IDMLResponse> UpdateAggregate(IAGBase aggregate) {
            var result = new DMLResponse<IAGBase>() { Success = false };
            await Q<TEntity>.SelectAllColumns().Where(u => u.ID == aggregate.IDProperty).ExecuteOne(cursor => {
                result = cursor.UpdateClause().UpsertAggregate(aggregate);
            }, () => result = new DMLResponse<IAGBase>() {
                Fault = new Exception($"Entity with ID: {aggregate.IDProperty} not found"),
                Success = false
            });
            return result;
        }

        public async Task<DMLResponse<TEntity>> SelectAll() {
            DMLResponse<TEntity> response = new DMLResponse<TEntity>() { Success = false };
            var list = new List<TEntity>();
            await Q<TEntity>.SelectAllColumns().ExecuteMany((query) => {
                list.Add(query.ResolvedEntity);
            });
            response = new DMLResponse<TEntity> { CollectionResponse = list };
            return response;
        }

        public async Task<DMLResponse<TEntity>> SelectBy(params Expression<Func<TEntity, bool>>[] selectors) {
            DMLResponse<TEntity> response = new DMLResponse<TEntity>() { Success = false };
            var list = new List<TEntity>();
            var confinement = Q<TEntity>.SelectAllColumns().Where(selectors.First());
            selectors.Skip(1).ToList().ForEach(sel => confinement = confinement.And(sel));
            await confinement.ExecuteMany((query) => {
                list.Add(query.ResolvedEntity);
            });
            response = new DMLResponse<TEntity> { CollectionResponse = list };
            return response;
        }

        public async Task<DMLResponse<TEntity>> SelectSingle(Expression<Func<TEntity, bool>> selector) {
            DMLResponse<TEntity> response = new DMLResponse<TEntity>() { Success = false };
            await Q<TEntity>.SelectAllColumns().Where(selector).ExecuteOne((query) => {
                response = new DMLResponse<TEntity> { SingleResponse = query.ResolvedEntity };
            });
            return response;
        }

        public async Task<DMLResponse<TEntity>> SelectLastRecord<TKey>(Expression<Func<TEntity, TKey>> keySelector) {
            DMLResponse<TEntity> response = new DMLResponse<TEntity>() { Success = false };
            await Q<TEntity>.SelectAllColumns().Order().By(keySelector, EOrderBy.ASC).ExecuteMany((query) => {
                response = new DMLResponse<TEntity> { SingleResponse = query.ResolvedEntity };
            });
            return response;
        }

        public async Task<DMLResponse<TEntity>> SelectSingleByID<TKey>(TKey key) {
            DMLResponse<TEntity> response = new DMLResponse<TEntity>() { Success = false };
            await Q<TEntity>.SelectAllColumns().Where(e => (object)e.ID == (object)key).ExecuteOne((query) => {
                response = new DMLResponse<TEntity> { SingleResponse = query.ResolvedEntity };
            });
            return response;
        }

        public async Task<IDMLResponse> GenericSelectAll() {
            return await SelectAll();
        }

        public IDMLResponse GenericSelectAllSync() {
            return SelectAllSync();
        }

        public async Task<IDMLResponse> GenericSelectByID(long id) {
            return await SelectSingleByID(id);
        }

        public IDMLResponse GenericSelectByIDSync(long id) {
            return SelectSingleByIDSync(id);
        }

        public async Task<IDMLResponse> GenericDelete(long id) {
            return await Delete(id);
        }

        public async Task<IDMLResponse> GenericUpdateAggregate(IAGBase aggregate) {
            return await UpdateAggregate(aggregate);
        }
    }
}
