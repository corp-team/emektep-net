﻿using HDS.App.Domain.Aggregate;
using HDS.App.Domain.Contracts;
using HDS.App.Domain.Objects;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace HDS.App.Engines.Core {
    internal class AGEngine<TAggregate> : AGSyncEngine<TAggregate>, IAGEngine<TAggregate>, IGenericEngine
        where TAggregate : AGBase<TAggregate> {

        public async Task<DMLResponse<TAggregate>> SelectAll() {
            return await Task.FromResult(SelectAllSync());
        }

        public async Task<DMLResponse<TAggregate>> SelectBy(params Expression<Func<TAggregate, bool>>[] selectors) {
            return await Task.FromResult(SelectBySync(selectors));
        }

        public async Task<DMLResponse<TAggregate>> SelectByID(long id) {
            return await Task.FromResult(SelectByIDSync(id));
        }

        public async Task<DMLResponse<TAggregate>> SelectSingle(Expression<Func<TAggregate, bool>> selector) {
            return await Task.FromResult(SelectSingleSync(selector));
        }

        public async Task<IDMLResponse> GenericSelectAll() {
            return await SelectAll();
        }

        public IDMLResponse GenericSelectAllSync() {
            return SelectAllSync();
        }

        public async Task<IDMLResponse> GenericSelectByID(long id) {
            return await SelectByID(id);
        }

        public IDMLResponse GenericSelectByIDSync(long id) {
            return SelectByIDSync(id);
        }

        public Task<IDMLResponse> GenericDelete(long id) {
            throw new NotImplementedException();
        }

        public Task<IDMLResponse> GenericUpdateAggregate(IAGBase aggregate) {
            throw new NotImplementedException();
        }
    }
}
